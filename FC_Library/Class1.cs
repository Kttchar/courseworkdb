﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
namespace FC_Library
{
    public class Class1
    {
    }

    public class FootballDbContext : DbContext
    {
        public FootballDbContext(string server, string dataBase, string username, string password)
        : base($"Server={server};Database={dataBase};User Id={username};Password={password};")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Transfer> Transfers { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<MatchPlayer> MatchPlayers { get; set; } 
        public DbSet<MatchCoach> MatchCoaches { get; set; }
    }






        public class User
    {
        public int UserID { get; set; }
        public string UserPublicID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public byte[] Avatar { get; set; }
    }

    public class Club
    {
        public int ClubID { get; set; }
        public string ClubName { get; set; }
        public string Country { get; set; }
        public string League { get; set; }
        public string Stadium { get; set; }
        public byte[] Logo { get; set; }
        public string UserPublicID { get; set; }
    }

    public class Player
    {
        public int PlayerID { get; set; }
        public string PlayerName { get; set; }
        public string PlayerSecondName { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public string Position { get; set; }
        public byte[] Photo { get; set; }
        public string UserPublicID { get; set; }
        public int ClubID { get; set; }
    }

    public class Agent
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string Agency { get; set; }
        public byte[] Photo { get; set; }
        public string UserPublicID { get; set; }
    }

    public class Transfer
    {
        public int TransferID { get; set; }
        public int PlayerID { get; set; }
        public int FromClubID { get; set; }
        public int ToClubID { get; set; }

        [Column(TypeName = "float")]
        public float TransferFee { get; set; }
        public DateTime TransferDate { get; set; }
        public string TransferStatus { get; set; }
    }

    public class Contract
    {
        public int ContractID { get; set; }
        public int PlayerID { get; set; }
        public int ClubID { get; set; }
        public int AgentID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [Column(TypeName = "float")]
        public float Salary { get; set; }
        public string ContractStatus { get; set; }
    }

    public class Coach
    {
        public int CoachID { get; set; }
        public string CoachName { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public byte[] Photo { get; set; }
        public int ClubID { get; set; }
    }

    public class Match
    {
        public int MatchID { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int HomeClubID { get; set; }
        public int AwayClubID { get; set; }
        public int HomeGoals { get; set; }
        public int AwayGoals { get; set; }
    }

    public class MatchPlayer
    {
        [Key, Column(Order = 0)]
        public int MatchID { get; set; }
        [Key, Column(Order = 1)]
        public int PlayerID { get; set; }
        public int ClubID { get; set; } 
        public string Position { get; set; }

        public virtual Match Match { get; set; }
        public virtual Player Player { get; set; }
        public virtual Club Club { get; set; } 
    }

    public class MatchCoach
    {
        [Key, Column(Order = 0)]
        public int MatchID { get; set; }
        [Key, Column(Order = 1)]
        public int CoachID { get; set; }
        public int ClubID { get; set; } 

        public virtual Match Match { get; set; }
        public virtual Coach Coach { get; set; }
        public virtual Club Club { get; set; } 
    }



}
