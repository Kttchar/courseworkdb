﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FC_Library;
using System.Windows.Forms;
using System.Data.Entity;
using MainForm.Controls.UserControls;
using MainForm.Controls.ClubControls;
using MainForm.Controls.PlayerControls;
using MainForm.Controls.AgentControls;
using MainForm.Controls.TransferControls;
using MainForm.Controls.ContractControls;

namespace MainForm
{
    public partial class MainFormFootballDB : Form
    {
        protected FootballDbContext _context;
        private LoginControl _loginUserControl;
        private ProfileControl _profileControl;
        private ChangeUserDataControl _changeUserDataControl;
        private UserDBChangeAddControl _userDBChangeAddControl;
        private ClubChangeAddControl _clubChangeAddControl;
        private PlayerChangeAddControl _playerChangeAddControl;
        private AgentChangeAddControl _agentChangeAddControl;
        private TransferChangeAddControl _transferChangeAddControl;
        private ContractChangeAddControl _contractChangeAddControl;
        //private ClubsTableControl _clubsTableControl;
        private Dictionary<string, UserControl> _tableControls;
        private TabPage _tabPageToHide;
        private string _server = "localhost";
        private string _dataBase = "football__db";
        private string _username = "default_user";
        private string _password = "11111111";

        public User CurrentUser { get; set; }

        public MainFormFootballDB()
        {
            InitializeComponent();

            _context = new FootballDbContext(_server, _dataBase, _username, _password); 

            _loginUserControl = new LoginControl(_context);
            _profileControl = new ProfileControl();
            _changeUserDataControl = new ChangeUserDataControl();
            _userDBChangeAddControl= new UserDBChangeAddControl();
            _clubChangeAddControl= new ClubChangeAddControl();
            _playerChangeAddControl = new PlayerChangeAddControl();
            _agentChangeAddControl = new AgentChangeAddControl();
            _transferChangeAddControl = new TransferChangeAddControl();
            _contractChangeAddControl = new ContractChangeAddControl();

            //_clubsTableControl = new ClubsTableControl();

            _loginUserControl.UserLoggedIn += SetCurrentUser;
            _profileControl.ChangeUserDataButtonClicked += (s, e) =>
            {
                
                _changeUserDataControl.Visible = true;
                _changeUserDataControl.BringToFront();
            };
            this.Controls.Add(_loginUserControl);
            this.Controls.Add(_profileControl);
            this.Controls.Add(_changeUserDataControl);
            this.Controls.Add(_userDBChangeAddControl);
            this.Controls.Add(_clubChangeAddControl);
            this.Controls.Add(_playerChangeAddControl);
            this.Controls.Add(_agentChangeAddControl);
            this.Controls.Add(_transferChangeAddControl);
            this.Controls.Add(_contractChangeAddControl);

            // Створіть словник для зберігання UserControl для кожної таблиці
            _tableControls = new Dictionary<string, UserControl>
        {
            { "Користувачі", new UsersTableControl() },
            { "Клуби", new ClubsTableControl() },
            { "Гравці", new PlayersTableControl() },
            { "Агенти", new AgentsTableControl() },
            { "Трансфери", new TransfersTableControl() },
            { "Контракти", new ContractsTableControl() },
            { "Тренери", new CoachesTableControl() },
            { "Матчі", new MatchesTableControl() }
            
        };

            // Додати вкладки таблиць до TabControl
            foreach (var tableName in _tableControls.Keys)
            {
                var tabPage = new TabPage(tableName);
                tabPage.Controls.Add(_tableControls[tableName]);
                TablesTabControl.TabPages.Add(tabPage);
            }
             _tabPageToHide = TablesTabControl.TabPages[0];

            TablesTabControl.TabPages.Remove(_tabPageToHide);

            


            //this.Controls.Add(_clubsTableControl);

            //_clubsTableControl.UpdateTable(); //оновлення представлення таблиці клубів
        }

        private void TablesTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Отримати вибрану таблицю
            string selectedTable = TablesTabControl.SelectedTab.Text;

            // Отримати відповідний UserControl
            UserControl control = _tableControls[selectedTable];
        }
        public void ChangeDatabaseUser(string server, string dataBase, string username, string password)
        {
            _server = server;
            _username = username;
            _password = password;
            _dataBase = dataBase;
            _context.Dispose();
            _context = new FootballDbContext(_server, _dataBase, _username, _password);
        }



        public void SetCurrentUser(User user = null)
        {
            if(user == null)
            {
                TablesTabControl.TabPages.Remove(_tabPageToHide);
                string server = "localhost";
                string dataBase = "football__db";
                string username = "default_user";
                string password = "11111111";
                ChangeDatabaseUser(server, dataBase, username, password);

                CurrentUser = user;

                
                _loginUserControl.Visible = false;
                _profileControl.Visible = false;
                _changeUserDataControl.Visible = false;

                idLabel.Visible= false;
                usernameTextBox.Visible = false;
                userIDTextBox.Visible = false;
                usernameTextBox.Text = "";
                userIDTextBox.Text = "";
                return;
            }
            else if(user.UserType == "administrator")
            {

                TablesTabControl.TabPages.Insert(0, _tabPageToHide);
                
                string server = "localhost";
                string dataBase = "football__db";
                string username = "default_admin";
                string password = "11111111";
                ChangeDatabaseUser(server, dataBase, username, password);  
            }
            else
            {
                TablesTabControl.TabPages.Remove(_tabPageToHide);
                string server = "localhost";
                string dataBase = "football__db";
                string username = "default_user";
                string password = "11111111";
                ChangeDatabaseUser(server, dataBase, username, password);
            }
            
            
            CurrentUser = user;
            _loginUserControl.Visible = false;
            _profileControl.DisplayUser(user);
            _changeUserDataControl.UpdateUser(user);
            // Оновити інтерфейс користувача
            usernameTextBox.Text = user.Username;
            userIDTextBox.Text = user.UserPublicID;
            idLabel.Visible = true;
            usernameTextBox.Visible = true;
            userIDTextBox.Visible = true;

        }

        public (string Server, string DBDataBase, string Username, string Password) GetConnectionParameters()
        {
            return (_server, _dataBase, _username, _password);
        }


        public void ShowError(string message)
        {
            MessageBox.Show(message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInfo(string message)
        {
            MessageBox.Show(message, "Повідомлення", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ShowUserDBChangeAddControl(User user = null)
        {
            //  новий UserDBChangeAddControl
            this.Controls.Remove(_userDBChangeAddControl);
            _userDBChangeAddControl = user == null ? new UserDBChangeAddControl() : new UserDBChangeAddControl(user);
            this.Controls.Add(_userDBChangeAddControl);
            _userDBChangeAddControl.Visible = true;
            _userDBChangeAddControl.BringToFront();
        }

        public void ShowClubChangeAddControl(Club club = null)
        {
            // новий ClubChangeAddControl
            this.Controls.Remove(_clubChangeAddControl);          
            _clubChangeAddControl = club == null ? new ClubChangeAddControl() : new ClubChangeAddControl(club);
            this.Controls.Add(_clubChangeAddControl);
            _clubChangeAddControl.Visible = true;
            _clubChangeAddControl.BringToFront();
        }

        public void ShowPlayerChangeAddControl(Player player = null)
        {
            // новий PlayerChangeAddControl
            this.Controls.Remove(_playerChangeAddControl);
            _playerChangeAddControl = player == null ? new PlayerChangeAddControl() : new PlayerChangeAddControl(player);
            this.Controls.Add(_playerChangeAddControl);
            _playerChangeAddControl.Visible = true;
            _playerChangeAddControl.BringToFront();
        }

        public void ShowAgentChangeAddControl(Agent agent = null)
        {
            // новий PlayerChangeAddControl
            this.Controls.Remove(_agentChangeAddControl);
            _agentChangeAddControl = agent == null ? new AgentChangeAddControl() : new AgentChangeAddControl(agent);
            this.Controls.Add(_agentChangeAddControl);
            _agentChangeAddControl.Visible = true;
            _agentChangeAddControl.BringToFront();
        }

        public void ShowTransferChangeAddControl(Transfer transfer = null)
        {
            // новий TransferChangeAddControl
            this.Controls.Remove(_transferChangeAddControl);
            _transferChangeAddControl = transfer == null ? new TransferChangeAddControl() : new TransferChangeAddControl(transfer);
            this.Controls.Add(_transferChangeAddControl);
            _transferChangeAddControl.Visible = true;
            _transferChangeAddControl.BringToFront();
        }

        public void ShowContractChangeAddControl(Contract contract = null)
        {
            // новий ContractChangeAddControl
            this.Controls.Remove(_contractChangeAddControl);
            _contractChangeAddControl = contract == null ? new ContractChangeAddControl() : new ContractChangeAddControl(contract);
            this.Controls.Add(_contractChangeAddControl);
            _contractChangeAddControl.Visible = true;
            _contractChangeAddControl.BringToFront();
        }


        private void ProfileButton_Click(object sender, EventArgs e)
        {
            if (CurrentUser != null)
            {
                _profileControl.Visible = true;
                _profileControl.BringToFront();
            }
            else
            {
                _loginUserControl.Visible = true;
                _loginUserControl.BringToFront();
            }
        }

        
    }
}
// UsersTableControl ClubsTableControl PlayersTableControl AgentsTableControl TransfersTableControl ContractsTableControl CoachesTableControl MatchesTableControl
// UserDBControl ClubControl PlayerControl AgentControl TransferControl ContractControl CoachControl MatchControl
// UserDBChangeAddControl ClubChangeAddControl PlayerChangeAddControl AgentChangeAddControl TransferChangeAddControl ContractChangeAddControl CoachChangeAddControl MatchChangeAddControl
/*
public MainForm()
    {
        InitializeComponent();

        // Створіть словник для зберігання UserControl для кожної таблиці
        _tableControls = new Dictionary<string, UserControl>
        {
            { "Users", new UsersTableControl() },
            { "Clubs", new ClubsTableControl() },
            { "Players", new PlayersTableControl() },
            { "Agents", new AgentsTableControl() },
            { "Transfers", new TransfersTableControl() },
            { "Contracts", new ContractsTableControl() },
            { "Coaches", new CoachesTableControl() },
            { "Matches", new MatchesTableControl() }
        };

        // Додайте вкладки до TabControl
        foreach (var tableName in _tableControls.Keys)
        {
            var tabPage = new TabPage(tableName);
            tabPage.Controls.Add(_tableControls[tableName]);
            tabControl1.TabPages.Add(tabPage);
        }

 private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Отримайте вибрану таблицю
        string selectedTable = tabControl1.SelectedTab.Text;

        // Отримайте відповідний UserControl
        UserControl control = _tableControls[selectedTable];

        // Відобразіть UserControl
        // ...
    }
*/