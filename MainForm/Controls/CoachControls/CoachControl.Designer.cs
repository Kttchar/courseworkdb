﻿namespace MainForm
{
    partial class CoachControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.CoachPhotoPictureBox = new System.Windows.Forms.PictureBox();
            this.NationalityLabel = new System.Windows.Forms.Label();
            this.AgeLabel = new System.Windows.Forms.Label();
            this.CoachNameLabel = new System.Windows.Forms.Label();
            this.ClubNameLabel = new System.Windows.Forms.Label();
            this.PlayersListBox = new System.Windows.Forms.ListBox();
            this.ClubLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CoachPhotoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClubLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(37, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "Команда тренера:";
            // 
            // CoachPhotoPictureBox
            // 
            this.CoachPhotoPictureBox.Location = new System.Drawing.Point(208, 7);
            this.CoachPhotoPictureBox.Name = "CoachPhotoPictureBox";
            this.CoachPhotoPictureBox.Size = new System.Drawing.Size(113, 88);
            this.CoachPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CoachPhotoPictureBox.TabIndex = 16;
            this.CoachPhotoPictureBox.TabStop = false;
            // 
            // NationalityLabel
            // 
            this.NationalityLabel.AutoSize = true;
            this.NationalityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NationalityLabel.Location = new System.Drawing.Point(79, 45);
            this.NationalityLabel.Name = "NationalityLabel";
            this.NationalityLabel.Size = new System.Drawing.Size(111, 18);
            this.NationalityLabel.TabIndex = 15;
            this.NationalityLabel.Text = "NationalityLabel";
            // 
            // AgeLabel
            // 
            this.AgeLabel.AutoSize = true;
            this.AgeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AgeLabel.Location = new System.Drawing.Point(79, 77);
            this.AgeLabel.Name = "AgeLabel";
            this.AgeLabel.Size = new System.Drawing.Size(68, 18);
            this.AgeLabel.TabIndex = 14;
            this.AgeLabel.Text = "AgeLabel";
            // 
            // CoachNameLabel
            // 
            this.CoachNameLabel.AutoSize = true;
            this.CoachNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CoachNameLabel.Location = new System.Drawing.Point(9, 7);
            this.CoachNameLabel.Name = "CoachNameLabel";
            this.CoachNameLabel.Size = new System.Drawing.Size(162, 24);
            this.CoachNameLabel.TabIndex = 13;
            this.CoachNameLabel.Text = "CoachNameLabel";
            // 
            // ClubNameLabel
            // 
            this.ClubNameLabel.AutoSize = true;
            this.ClubNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClubNameLabel.Location = new System.Drawing.Point(77, 125);
            this.ClubNameLabel.Name = "ClubNameLabel";
            this.ClubNameLabel.Size = new System.Drawing.Size(113, 18);
            this.ClubNameLabel.TabIndex = 18;
            this.ClubNameLabel.Text = "ClubNameLabel";
            // 
            // PlayersListBox
            // 
            this.PlayersListBox.FormattingEnabled = true;
            this.PlayersListBox.Location = new System.Drawing.Point(13, 190);
            this.PlayersListBox.Name = "PlayersListBox";
            this.PlayersListBox.Size = new System.Drawing.Size(189, 108);
            this.PlayersListBox.TabIndex = 19;
            // 
            // ClubLogoPictureBox
            // 
            this.ClubLogoPictureBox.Location = new System.Drawing.Point(208, 125);
            this.ClubLogoPictureBox.Name = "ClubLogoPictureBox";
            this.ClubLogoPictureBox.Size = new System.Drawing.Size(113, 88);
            this.ClubLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ClubLogoPictureBox.TabIndex = 20;
            this.ClubLogoPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label1.Location = new System.Drawing.Point(26, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 18);
            this.label1.TabIndex = 24;
            this.label1.Text = "Клуб:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label2.Location = new System.Drawing.Point(23, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 18);
            this.label2.TabIndex = 22;
            this.label2.Text = "Нація:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label5.Location = new System.Drawing.Point(37, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 18);
            this.label5.TabIndex = 21;
            this.label5.Text = "Вік:";
            // 
            // CoachControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ClubLogoPictureBox);
            this.Controls.Add(this.PlayersListBox);
            this.Controls.Add(this.ClubNameLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CoachPhotoPictureBox);
            this.Controls.Add(this.NationalityLabel);
            this.Controls.Add(this.AgeLabel);
            this.Controls.Add(this.CoachNameLabel);
            this.Name = "CoachControl";
            this.Size = new System.Drawing.Size(329, 305);
            ((System.ComponentModel.ISupportInitialize)(this.CoachPhotoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClubLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox CoachPhotoPictureBox;
        private System.Windows.Forms.Label NationalityLabel;
        private System.Windows.Forms.Label AgeLabel;
        private System.Windows.Forms.Label CoachNameLabel;
        private System.Windows.Forms.Label ClubNameLabel;
        private System.Windows.Forms.ListBox PlayersListBox;
        private System.Windows.Forms.PictureBox ClubLogoPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
    }
}
