﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class CoachControl : UserControl
    {
        private Coach _coach;
        private Club _club;
        private List<Player> _players;

        public CoachControl(Coach coach, Club club, List<Player> players)
        {
            InitializeComponent();
            
            this.BackColor = Color.WhiteSmoke;

            _coach = coach;
            _club = club;
            _players = players;

            LoadData();
        }

        private void LoadData()
        {
            // Завантажте дані тренера, клубу та пов'язані дані до відповідних елементів керування
            CoachNameLabel.Text = _coach.CoachName;
            NationalityLabel.Text = _coach.Nationality;
            AgeLabel.Text = _coach.Age.ToString(); // додано цей рядок
            ClubNameLabel.Text = _club.ClubName;

            if (_coach.Photo != null)
            {
                using (var ms = new MemoryStream(_coach.Photo))
                {
                    CoachPhotoPictureBox.Image = Image.FromStream(ms);
                }
            }

            if (_club.Logo != null)
            {
                using (var ms = new MemoryStream(_club.Logo))
                {
                    ClubLogoPictureBox.Image = Image.FromStream(ms);
                }
            }

            foreach (var player in _players)
            {
                PlayersListBox.Items.Add(player.PlayerName + " " + player.PlayerSecondName);
            }
        }
    }
}
