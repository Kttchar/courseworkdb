﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class CoachesTableControl : UserControl
    {
        public CoachesTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                var coaches = context.Coaches.ToList();
                CoachesFlowLayoutPanel.Controls.Clear();

                string searchText = SearchTextBox.Text.ToLower();

                foreach (var coach in coaches)
                {
                    var players = context.Players.Where(p => p.ClubID == coach.ClubID).ToList();
                    var club = context.Clubs.FirstOrDefault(c => c.ClubID == coach.ClubID);

                    if (string.IsNullOrEmpty(searchText) ||
                        coach.CoachName.ToLower().Contains(searchText) ||
                        coach.Nationality.ToLower().Contains(searchText) ||
                        (club != null && club.ClubName.ToLower().Contains(searchText)) ||
                        players.Any(p => (p.PlayerName + " " + p.PlayerSecondName).ToLower().Contains(searchText)))
                    {

                        CoachControl coachControl = new CoachControl(coach,club, players);
                        CoachesFlowLayoutPanel.Controls.Add(coachControl);
                    }
                }
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void CoachesTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }
    }
}
