﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm.Controls.ClubControls
{
    public partial class ClubChangeAddControl : UserControl
    {
        private Club _club;
        public ClubChangeAddControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            // Зміна тексту лейбелів та кнопки
            ClubNameLabel.Text = "Нова назву клубу:";
            CountryLabel.Text = "Нова країна:";
            LeagueLabel.Text = "Нова ліга:";
            StadiumLabel.Text = "Новий стадіон:";
            ChangeAddButton.Text = "Додати клуб";

        }

        public ClubChangeAddControl(Club club) : this()
        {
            _club = club;
            // Зміна тексту лейбелів та кнопки
            ClubNameLabel.Text = "Змінити назву клубу:";
            CountryLabel.Text = "Змінити країну:";
            LeagueLabel.Text = "Змінити лігу:";
            StadiumLabel.Text = "Змінити стадіон:";
            ChangeAddButton.Text = "Змінити клуб";
            // Вставка даних клубу в текстбокси
            ClubNameTextBox.Text = _club.ClubName;
            CountryTextBox.Text = _club.Country;
            LeagueTextBox.Text = _club.League;
            StadiumTextBox.Text = _club.Stadium;
            UserPublicIDTextBox.Text = _club.UserPublicID;
            // Вставка логотипу клубу
            if (_club.Logo != null)
            {
                using (var ms = new MemoryStream(_club.Logo))
                {
                    LogoPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private async void ChangeAddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                //перевірка при додаванні нового клубу
                if (form.CurrentUser.UserType != "administrator" && _club == null &&
                    (form.CurrentUser.UserType != "club" ||
                     (form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    form.ShowError("У вас немає прав на додавання нових клубів!");
                    return;
                }

                //перевірка при редагуванні клубу
                if (form.CurrentUser.UserType != "administrator" && _club != null &&
                    (form.CurrentUser.UserType != "club" ||
                     (form.CurrentUser.UserType == "club" && form.CurrentUser.UserPublicID != _club.UserPublicID)))
                {
                    form.ShowError("У вас немає прав на редагування цього клубу!");
                    return;
                }

                // Перевірка, чи всі поля заповнені
                if (string.IsNullOrEmpty(ClubNameTextBox.Text) ||
                    string.IsNullOrEmpty(CountryTextBox.Text) ||
                    string.IsNullOrEmpty(LeagueTextBox.Text) ||
                    string.IsNullOrEmpty(StadiumTextBox.Text))
                {
                    form.ShowError("Будь ласка, заповніть всі поля!");
                    return;
                }

                byte[] logoBytes = null;
                if (LogoPictureBox.Image != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        LogoPictureBox.Image.Save(ms, LogoPictureBox.Image.RawFormat);
                        logoBytes = ms.ToArray();
                    }
                }


                // Перевірка, чи існує користувач з таким публічним айді
                var existingUser = await context.Users.FirstOrDefaultAsync(u => u.UserPublicID == UserPublicIDTextBox.Text);

                if (existingUser == null)
                {
                    form.ShowError("Користувача з таким публічним ID не існує!");
                    return;
                }

                // Перевірка, чи існує клуб з таким ім'ям
                var existingClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubName == ClubNameTextBox.Text);
                if (existingClub != null && (_club == null || existingClub.ClubID != _club.ClubID))
                {
                    form.ShowError("Клуб з таким ім'ям вже існує!");
                    return;
                }

                if (_club == null)
                {
                    // Виклик процедури AddClubByUserID
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC AddClubByUserID @p0, @p1, @p2, @p3, @p4, @p5",
                        ClubNameTextBox.Text,
                        CountryTextBox.Text,
                        LeagueTextBox.Text,
                        StadiumTextBox.Text,
                        logoBytes,
                        UserPublicIDTextBox.Text
                    );
                    form.ShowInfo("Клуб успішно доданий!");
                }
                else
                {
                    // Виклик процедури ChangeClubData
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC ChangeClubData @p0, @p1, @p2, @p3, @p4, @p5, @p6",
                        _club.ClubID,
                        ClubNameTextBox.Text,
                        CountryTextBox.Text,
                        LeagueTextBox.Text,
                        StadiumTextBox.Text,
                        logoBytes,
                        UserPublicIDTextBox.Text
                    );
                    form.ShowInfo("Дані клубу успішно змінено!");
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void ClubChangeAddControl_Load(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && _club == null)
                UserPublicIDTextBox.Text = form.CurrentUser.UserPublicID;
        }

        private void selectPhotoButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Image Files( *.gif; *.bmp; *.png)|*.gif; *.bmp; *.png";
                if (ofd.ShowDialog() == DialogResult.OK)
                {


                    LogoPictureBox.Image = Image.FromFile(ofd.FileName);
                }
            }
        }
    }
}
