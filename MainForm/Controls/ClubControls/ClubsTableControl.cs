﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FC_Library;
using System.Windows.Forms;
using System.Runtime.Remoting.Contexts;

namespace MainForm
{
    public partial class ClubsTableControl : UserControl
    {
        public ClubsTableControl()
        {
            InitializeComponent();

            this.Dock = DockStyle.Fill;
        }
        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Отримати всі клуби з таблиці клубів

                var clubs = context.Clubs.ToList();

                // Очистити FlowLayoutPanel
                ClubsTableFlowPanel.Controls.Clear();
                // Створити новий ClubControl для кожного клубу і додати його до FlowLayoutPanel

                string searchText = SearchTextBox.Text.ToLower();

                foreach (var club in clubs)
                {                    
                    if (string.IsNullOrEmpty(searchText) ||
                        club.ClubName.ToLower().Contains(searchText) ||
                        club.Country.ToLower().Contains(searchText) ||
                        club.League.ToLower().Contains(searchText) ||
                        club.Stadium.ToLower().Contains(searchText))
                    {
                        ClubControl clubControl = new ClubControl(club);
                        ClubsTableFlowPanel.Controls.Add(clubControl);
                    }
                }
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void ClubsTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void SearchTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void ClubsTableFlowPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                if (form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" || !(form.CurrentUser.UserType != "administrator"  &&
                    (form.CurrentUser.UserType != "club" ||
                     (form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))))))
                {
                    form.ShowClubChangeAddControl();
                }
                else
                {
                    form.ShowError("У вас немає прав на додавання клубів!");
                }
            }
        }
    }
}
