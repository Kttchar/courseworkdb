﻿namespace MainForm.Controls.ClubControls
{
    partial class ClubChangeAddControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserPublicIDLabel = new System.Windows.Forms.Label();
            this.UserPublicIDTextBox = new System.Windows.Forms.TextBox();
            this.LogoPictureBox = new System.Windows.Forms.PictureBox();
            this.selectPhotoButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.CountryLabel = new System.Windows.Forms.Label();
            this.ClubNameLabel = new System.Windows.Forms.Label();
            this.CountryTextBox = new System.Windows.Forms.TextBox();
            this.ClubNameTextBox = new System.Windows.Forms.TextBox();
            this.ChangeAddButton = new System.Windows.Forms.Button();
            this.LeagueLabel = new System.Windows.Forms.Label();
            this.LeagueTextBox = new System.Windows.Forms.TextBox();
            this.StadiumLabel = new System.Windows.Forms.Label();
            this.StadiumTextBox = new System.Windows.Forms.TextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // UserPublicIDLabel
            // 
            this.UserPublicIDLabel.AutoSize = true;
            this.UserPublicIDLabel.Location = new System.Drawing.Point(30, 223);
            this.UserPublicIDLabel.Name = "UserPublicIDLabel";
            this.UserPublicIDLabel.Size = new System.Drawing.Size(140, 13);
            this.UserPublicIDLabel.TabIndex = 41;
            this.UserPublicIDLabel.Text = "Публічний ID користувача:";
            // 
            // UserPublicIDTextBox
            // 
            this.UserPublicIDTextBox.Location = new System.Drawing.Point(176, 220);
            this.UserPublicIDTextBox.Name = "UserPublicIDTextBox";
            this.UserPublicIDTextBox.Size = new System.Drawing.Size(121, 20);
            this.UserPublicIDTextBox.TabIndex = 40;
            // 
            // LogoPictureBox
            // 
            this.LogoPictureBox.Location = new System.Drawing.Point(324, 50);
            this.LogoPictureBox.Name = "LogoPictureBox";
            this.LogoPictureBox.Size = new System.Drawing.Size(166, 145);
            this.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPictureBox.TabIndex = 37;
            this.LogoPictureBox.TabStop = false;
            // 
            // selectPhotoButton
            // 
            this.selectPhotoButton.Location = new System.Drawing.Point(338, 201);
            this.selectPhotoButton.Name = "selectPhotoButton";
            this.selectPhotoButton.Size = new System.Drawing.Size(134, 23);
            this.selectPhotoButton.TabIndex = 36;
            this.selectPhotoButton.Text = "Вибрати аватар";
            this.selectPhotoButton.UseVisualStyleBackColor = true;
            this.selectPhotoButton.Click += new System.EventHandler(this.selectPhotoButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(380, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Аватар:";
            // 
            // CountryLabel
            // 
            this.CountryLabel.AutoSize = true;
            this.CountryLabel.Location = new System.Drawing.Point(30, 110);
            this.CountryLabel.Name = "CountryLabel";
            this.CountryLabel.Size = new System.Drawing.Size(69, 13);
            this.CountryLabel.TabIndex = 34;
            this.CountryLabel.Text = "CountryLabel";
            // 
            // ClubNameLabel
            // 
            this.ClubNameLabel.AutoSize = true;
            this.ClubNameLabel.Location = new System.Drawing.Point(18, 78);
            this.ClubNameLabel.Name = "ClubNameLabel";
            this.ClubNameLabel.Size = new System.Drawing.Size(70, 13);
            this.ClubNameLabel.TabIndex = 33;
            this.ClubNameLabel.Text = "Новий логін:";
            // 
            // CountryTextBox
            // 
            this.CountryTextBox.Location = new System.Drawing.Point(160, 107);
            this.CountryTextBox.Name = "CountryTextBox";
            this.CountryTextBox.Size = new System.Drawing.Size(121, 20);
            this.CountryTextBox.TabIndex = 32;
            // 
            // ClubNameTextBox
            // 
            this.ClubNameTextBox.Location = new System.Drawing.Point(160, 75);
            this.ClubNameTextBox.Name = "ClubNameTextBox";
            this.ClubNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.ClubNameTextBox.TabIndex = 31;
            // 
            // ChangeAddButton
            // 
            this.ChangeAddButton.Location = new System.Drawing.Point(163, 262);
            this.ChangeAddButton.Name = "ChangeAddButton";
            this.ChangeAddButton.Size = new System.Drawing.Size(134, 23);
            this.ChangeAddButton.TabIndex = 30;
            this.ChangeAddButton.Text = "Додати користувача";
            this.ChangeAddButton.UseVisualStyleBackColor = true;
            this.ChangeAddButton.Click += new System.EventHandler(this.ChangeAddButton_Click);
            // 
            // LeagueLabel
            // 
            this.LeagueLabel.AutoSize = true;
            this.LeagueLabel.Location = new System.Drawing.Point(30, 149);
            this.LeagueLabel.Name = "LeagueLabel";
            this.LeagueLabel.Size = new System.Drawing.Size(69, 13);
            this.LeagueLabel.TabIndex = 43;
            this.LeagueLabel.Text = "LeagueLabel";
            // 
            // LeagueTextBox
            // 
            this.LeagueTextBox.Location = new System.Drawing.Point(160, 146);
            this.LeagueTextBox.Name = "LeagueTextBox";
            this.LeagueTextBox.Size = new System.Drawing.Size(121, 20);
            this.LeagueTextBox.TabIndex = 42;
            // 
            // StadiumLabel
            // 
            this.StadiumLabel.AutoSize = true;
            this.StadiumLabel.Location = new System.Drawing.Point(30, 185);
            this.StadiumLabel.Name = "StadiumLabel";
            this.StadiumLabel.Size = new System.Drawing.Size(71, 13);
            this.StadiumLabel.TabIndex = 45;
            this.StadiumLabel.Text = "StadiumLabel";
            // 
            // StadiumTextBox
            // 
            this.StadiumTextBox.Location = new System.Drawing.Point(160, 185);
            this.StadiumTextBox.Name = "StadiumTextBox";
            this.StadiumTextBox.Size = new System.Drawing.Size(121, 20);
            this.StadiumTextBox.TabIndex = 44;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(467, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(37, 18);
            this.CloseButton.TabIndex = 46;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ClubChangeAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.StadiumLabel);
            this.Controls.Add(this.StadiumTextBox);
            this.Controls.Add(this.LeagueLabel);
            this.Controls.Add(this.LeagueTextBox);
            this.Controls.Add(this.UserPublicIDLabel);
            this.Controls.Add(this.UserPublicIDTextBox);
            this.Controls.Add(this.LogoPictureBox);
            this.Controls.Add(this.selectPhotoButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CountryLabel);
            this.Controls.Add(this.ClubNameLabel);
            this.Controls.Add(this.CountryTextBox);
            this.Controls.Add(this.ClubNameTextBox);
            this.Controls.Add(this.ChangeAddButton);
            this.Name = "ClubChangeAddControl";
            this.Size = new System.Drawing.Size(504, 308);
            this.Load += new System.EventHandler(this.ClubChangeAddControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UserPublicIDLabel;
        private System.Windows.Forms.TextBox UserPublicIDTextBox;
        private System.Windows.Forms.PictureBox LogoPictureBox;
        private System.Windows.Forms.Button selectPhotoButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label CountryLabel;
        private System.Windows.Forms.Label ClubNameLabel;
        private System.Windows.Forms.TextBox CountryTextBox;
        private System.Windows.Forms.TextBox ClubNameTextBox;
        private System.Windows.Forms.Button ChangeAddButton;
        private System.Windows.Forms.Label LeagueLabel;
        private System.Windows.Forms.TextBox LeagueTextBox;
        private System.Windows.Forms.Label StadiumLabel;
        private System.Windows.Forms.TextBox StadiumTextBox;
        private System.Windows.Forms.Button CloseButton;
    }
}
