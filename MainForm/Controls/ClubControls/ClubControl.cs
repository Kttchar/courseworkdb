﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{

    public partial class ClubControl : UserControl
    {
        private Club _club;
        public ClubControl(Club club)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _club = club;

            clubNameLabel.Text = _club.ClubName;
            countryLabel.Text = _club.Country;
            leagueLabel.Text = _club.League;
            stadiumLabel.Text = _club.Stadium;

            if (_club.Logo != null)
            {
                using (var ms = new MemoryStream(_club.Logo))
                {
                    logoPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private void ClubControl_DoubleClick(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" || !(form.CurrentUser.UserType != "administrator" && _club != null &&
                    (form.CurrentUser.UserType != "club" ||
                     (form.CurrentUser.UserType == "club" && form.CurrentUser.UserPublicID != _club.UserPublicID)))))
            {
                form.ShowClubChangeAddControl(_club);
            }
        }
    }
}
