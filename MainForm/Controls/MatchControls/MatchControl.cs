﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FC_Library;

namespace MainForm
{
    public partial class MatchControl : UserControl
    {
        private Match _match;
        private Club _homeClub;
        private Club _awayClub;
        private List<MatchPlayer> _matchPlayers;
        private List<MatchCoach> _matchCoaches;

        public MatchControl(Match match, Club homeClub, Club awayClub, List<MatchPlayer> matchPlayers, List<MatchCoach> matchCoaches)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _match = match;
            _homeClub = homeClub;
            _awayClub = awayClub;
            _matchPlayers = matchPlayers;
            _matchCoaches = matchCoaches;
            LoadData();
        }

        private void LoadData()
        {
            DateLabel.Text = _match.Date.ToShortDateString();
            StartTimeLabel.Text = _match.StartTime.ToString();
            EndTimeLabel.Text = _match.EndTime.ToString();
            HomeClubLabel.Text = _homeClub.ClubName;
            AwayClubLabel.Text = _awayClub.ClubName;
            HomeGoalsLabel.Text = _match.HomeGoals.ToString();
            AwayGoalsLabel.Text = _match.AwayGoals.ToString();

            foreach (var player in _matchPlayers.Where(mp => mp.Match.HomeClubID == mp.ClubID))
            {
                HomePlayersListBox.Items.Add($"{player.Player.PlayerName} {player.Player.PlayerSecondName}\t{player.Position}");
            }

            foreach (var player in _matchPlayers.Where(mp => mp.Match.AwayClubID == mp.ClubID))
            {
                AwayPlayersListBox.Items.Add($"{player.Player.PlayerName} {player.Player.PlayerSecondName}\t{player.Position}");
            }

            HomeCoachLabel.Text = _matchCoaches.FirstOrDefault(mc => mc.Match.HomeClubID == mc.ClubID)?.Coach.CoachName;
            AwayCoachLabel.Text = _matchCoaches.FirstOrDefault(mc => mc.Match.AwayClubID == mc.ClubID)?.Coach.CoachName;

            if (_homeClub.Logo != null)
            {
                using (var ms = new MemoryStream(_homeClub.Logo))
                {
                    HomeClubLogoPictureBox.Image = Image.FromStream(ms);
                }
            }

            if (_awayClub.Logo != null)
            {
                using (var ms = new MemoryStream(_awayClub.Logo))
                {
                    AwayClubLogoPictureBox.Image = Image.FromStream(ms);
                }
            }

            if (_match.HomeGoals > _match.AwayGoals)
            {
                HomeClubLabel.ForeColor = Color.Green;
                HomeGoalsLabel.ForeColor = Color.Green;
                AwayClubLabel.ForeColor = Color.Red;
                AwayGoalsLabel.ForeColor = Color.Red;
            }
            else if (_match.HomeGoals < _match.AwayGoals)
            {
                HomeClubLabel.ForeColor = Color.Red;
                HomeGoalsLabel.ForeColor = Color.Red;
                AwayClubLabel.ForeColor = Color.Green;
                AwayGoalsLabel.ForeColor = Color.Green;
            }
            else
            {
                HomeClubLabel.ForeColor = Color.DarkOrange;
                HomeGoalsLabel.ForeColor = Color.DarkOrange;
                AwayClubLabel.ForeColor = Color.DarkOrange;
                AwayGoalsLabel.ForeColor = Color.DarkOrange;
            }
        }
    }
}
