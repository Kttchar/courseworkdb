﻿namespace MainForm
{
    partial class MatchControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.HomePlayersListBox = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.HomeClubLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.EndTimeLabel = new System.Windows.Forms.Label();
            this.AwayClubLabel = new System.Windows.Forms.Label();
            this.StartTimeLabel = new System.Windows.Forms.Label();
            this.HomeGoalsLabel = new System.Windows.Forms.Label();
            this.AwayGoalsLabel = new System.Windows.Forms.Label();
            this.AwayPlayersListBox = new System.Windows.Forms.ListBox();
            this.AwayCoachLabel = new System.Windows.Forms.Label();
            this.HomeCoachLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DateLabel = new System.Windows.Forms.Label();
            this.HomeClubLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.AwayClubLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.HomeClubLogoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AwayClubLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // HomePlayersListBox
            // 
            this.HomePlayersListBox.FormattingEnabled = true;
            this.HomePlayersListBox.Location = new System.Drawing.Point(15, 306);
            this.HomePlayersListBox.MultiColumn = true;
            this.HomePlayersListBox.Name = "HomePlayersListBox";
            this.HomePlayersListBox.Size = new System.Drawing.Size(164, 108);
            this.HomePlayersListBox.Sorted = true;
            this.HomePlayersListBox.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 23;
            this.label4.Text = "Команда:";
            // 
            // HomeClubLabel
            // 
            this.HomeClubLabel.AutoSize = true;
            this.HomeClubLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HomeClubLabel.Location = new System.Drawing.Point(21, 66);
            this.HomeClubLabel.Name = "HomeClubLabel";
            this.HomeClubLabel.Size = new System.Drawing.Size(147, 24);
            this.HomeClubLabel.TabIndex = 20;
            this.HomeClubLabel.Text = "HomeClubLabel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(211, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 18);
            this.label2.TabIndex = 28;
            this.label2.Text = "Команда:";
            // 
            // EndTimeLabel
            // 
            this.EndTimeLabel.AutoSize = true;
            this.EndTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EndTimeLabel.Location = new System.Drawing.Point(230, 175);
            this.EndTimeLabel.Name = "EndTimeLabel";
            this.EndTimeLabel.Size = new System.Drawing.Size(102, 18);
            this.EndTimeLabel.TabIndex = 27;
            this.EndTimeLabel.Text = "EndTimeLabel";
            // 
            // AwayClubLabel
            // 
            this.AwayClubLabel.AutoSize = true;
            this.AwayClubLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AwayClubLabel.Location = new System.Drawing.Point(210, 66);
            this.AwayClubLabel.Name = "AwayClubLabel";
            this.AwayClubLabel.Size = new System.Drawing.Size(141, 24);
            this.AwayClubLabel.TabIndex = 30;
            this.AwayClubLabel.Text = "AwayClubLabel";
            // 
            // StartTimeLabel
            // 
            this.StartTimeLabel.AutoSize = true;
            this.StartTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartTimeLabel.Location = new System.Drawing.Point(40, 175);
            this.StartTimeLabel.Name = "StartTimeLabel";
            this.StartTimeLabel.Size = new System.Drawing.Size(107, 18);
            this.StartTimeLabel.TabIndex = 31;
            this.StartTimeLabel.Text = "StartTimeLabel";
            // 
            // HomeGoalsLabel
            // 
            this.HomeGoalsLabel.AutoSize = true;
            this.HomeGoalsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HomeGoalsLabel.Location = new System.Drawing.Point(90, 111);
            this.HomeGoalsLabel.Name = "HomeGoalsLabel";
            this.HomeGoalsLabel.Size = new System.Drawing.Size(156, 24);
            this.HomeGoalsLabel.TabIndex = 32;
            this.HomeGoalsLabel.Text = "HomeGoalsLabel";
            // 
            // AwayGoalsLabel
            // 
            this.AwayGoalsLabel.AutoSize = true;
            this.AwayGoalsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AwayGoalsLabel.Location = new System.Drawing.Point(280, 111);
            this.AwayGoalsLabel.Name = "AwayGoalsLabel";
            this.AwayGoalsLabel.Size = new System.Drawing.Size(150, 24);
            this.AwayGoalsLabel.TabIndex = 33;
            this.AwayGoalsLabel.Text = "AwayGoalsLabel";
            // 
            // AwayPlayersListBox
            // 
            this.AwayPlayersListBox.FormattingEnabled = true;
            this.AwayPlayersListBox.Location = new System.Drawing.Point(212, 306);
            this.AwayPlayersListBox.MultiColumn = true;
            this.AwayPlayersListBox.Name = "AwayPlayersListBox";
            this.AwayPlayersListBox.Size = new System.Drawing.Size(164, 108);
            this.AwayPlayersListBox.Sorted = true;
            this.AwayPlayersListBox.TabIndex = 34;
            // 
            // AwayCoachLabel
            // 
            this.AwayCoachLabel.AutoSize = true;
            this.AwayCoachLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AwayCoachLabel.Location = new System.Drawing.Point(211, 258);
            this.AwayCoachLabel.Name = "AwayCoachLabel";
            this.AwayCoachLabel.Size = new System.Drawing.Size(122, 18);
            this.AwayCoachLabel.TabIndex = 26;
            this.AwayCoachLabel.Text = "AwayCoachLabel";
            // 
            // HomeCoachLabel
            // 
            this.HomeCoachLabel.AutoSize = true;
            this.HomeCoachLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HomeCoachLabel.Location = new System.Drawing.Point(22, 258);
            this.HomeCoachLabel.Name = "HomeCoachLabel";
            this.HomeCoachLabel.Size = new System.Drawing.Size(128, 18);
            this.HomeCoachLabel.TabIndex = 21;
            this.HomeCoachLabel.Text = "HomeCoachLabel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(174, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 24);
            this.label1.TabIndex = 35;
            this.label1.Text = "VS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(45, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 18);
            this.label3.TabIndex = 36;
            this.label3.Text = "Початок:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(251, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 18);
            this.label6.TabIndex = 37;
            this.label6.Text = "Кінець:";
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DateLabel.Location = new System.Drawing.Point(143, 204);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(94, 24);
            this.DateLabel.TabIndex = 38;
            this.DateLabel.Text = "DateLabel";
            // 
            // HomeClubLogoPictureBox
            // 
            this.HomeClubLogoPictureBox.Location = new System.Drawing.Point(25, 12);
            this.HomeClubLogoPictureBox.Name = "HomeClubLogoPictureBox";
            this.HomeClubLogoPictureBox.Size = new System.Drawing.Size(143, 51);
            this.HomeClubLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.HomeClubLogoPictureBox.TabIndex = 39;
            this.HomeClubLogoPictureBox.TabStop = false;
            // 
            // AwayClubLogoPictureBox
            // 
            this.AwayClubLogoPictureBox.Location = new System.Drawing.Point(214, 12);
            this.AwayClubLogoPictureBox.Name = "AwayClubLogoPictureBox";
            this.AwayClubLogoPictureBox.Size = new System.Drawing.Size(143, 51);
            this.AwayClubLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AwayClubLogoPictureBox.TabIndex = 40;
            this.AwayClubLogoPictureBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(153, 240);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 18);
            this.label5.TabIndex = 41;
            this.label5.Text = "Тренери:";
            // 
            // MatchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AwayClubLogoPictureBox);
            this.Controls.Add(this.HomeClubLogoPictureBox);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AwayPlayersListBox);
            this.Controls.Add(this.AwayGoalsLabel);
            this.Controls.Add(this.HomeGoalsLabel);
            this.Controls.Add(this.StartTimeLabel);
            this.Controls.Add(this.AwayClubLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.EndTimeLabel);
            this.Controls.Add(this.AwayCoachLabel);
            this.Controls.Add(this.HomePlayersListBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.HomeCoachLabel);
            this.Controls.Add(this.HomeClubLabel);
            this.Name = "MatchControl";
            this.Size = new System.Drawing.Size(433, 425);
            ((System.ComponentModel.ISupportInitialize)(this.HomeClubLogoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AwayClubLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox HomePlayersListBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label HomeClubLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label EndTimeLabel;
        private System.Windows.Forms.Label AwayClubLabel;
        private System.Windows.Forms.Label StartTimeLabel;
        private System.Windows.Forms.Label HomeGoalsLabel;
        private System.Windows.Forms.Label AwayGoalsLabel;
        private System.Windows.Forms.ListBox AwayPlayersListBox;
        private System.Windows.Forms.Label AwayCoachLabel;
        private System.Windows.Forms.Label HomeCoachLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label DateLabel;
        private System.Windows.Forms.PictureBox HomeClubLogoPictureBox;
        private System.Windows.Forms.PictureBox AwayClubLogoPictureBox;
        private System.Windows.Forms.Label label5;
    }
}
