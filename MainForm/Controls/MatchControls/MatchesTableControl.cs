﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class MatchesTableControl : UserControl
    {
        public MatchesTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                var matches = context.Matches.ToList();
                MatchesFlowLayoutPanel.Controls.Clear();

                string searchText = SearchTextBox.Text.ToLower();
                foreach (var match in matches)
                {
                    var homeClub = context.Clubs.FirstOrDefault(c => c.ClubID == match.HomeClubID);
                    var awayClub = context.Clubs.FirstOrDefault(c => c.ClubID == match.AwayClubID);
                    var matchPlayers = context.MatchPlayers.Where(mp => mp.MatchID == match.MatchID).ToList();
                    var matchCoaches = context.MatchCoaches.Where(mc => mc.MatchID == match.MatchID).ToList();

                    if (string.IsNullOrEmpty(searchText) ||
                        homeClub.ClubName.ToLower().Contains(searchText) ||
                        awayClub.ClubName.ToLower().Contains(searchText) ||
                        matchPlayers.Any(mp => (mp.Player.PlayerName + " " + mp.Player.PlayerSecondName).ToLower().Contains(searchText)) ||
                        matchCoaches.Any(mc => mc.Coach.CoachName.ToLower().Contains(searchText)))
                    {
                        var matchControl = new MatchControl(match, homeClub, awayClub, matchPlayers, matchCoaches);
                        MatchesFlowLayoutPanel.Controls.Add(matchControl);
                    }
                }
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void MatchesTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }
    }
}
