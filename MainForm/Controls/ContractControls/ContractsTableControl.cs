﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ContractsTableControl : UserControl
    {
        public ContractsTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Отримати всі контракти з таблиці контрактів
                var contracts = context.Contracts.ToList();

                // Очистити FlowLayoutPanel
                ContractsTableFlowPanel.Controls.Clear();

                // Створити новий ContractControl для кожного контракту і додати його до FlowLayoutPanel
                string searchText = SearchTextBox.Text.ToLower();

                foreach (var contract in contracts)
                {
                    // Отримати гравця, клуб та агента за PlayerID, ClubID та AgentID
                    var player = context.Players.FirstOrDefault(p => p.PlayerID == contract.PlayerID);
                    var club = context.Clubs.FirstOrDefault(c => c.ClubID == contract.ClubID);
                    var agent = context.Agents.FirstOrDefault(a => a.AgentID == contract.AgentID);

                    string playerName = player != null ? player.PlayerName + " " + player.PlayerSecondName : "";
                    string clubName = club != null ? club.ClubName : "";
                    string agentName = agent != null ? agent.AgentName : "";

                    if (string.IsNullOrEmpty(searchText) ||
                        playerName.ToLower().Contains(searchText) ||
                        clubName.ToLower().Contains(searchText) ||
                        agentName.ToLower().Contains(searchText) ||
                        contract.ContractStatus.ToLower().Contains(searchText) ||
                        contract.StartDate.ToString().Contains(searchText) ||
                        contract.EndDate.ToString().Contains(searchText) ||
                        contract.Salary.ToString().Contains(searchText))
                    {
                        ContractControl contractControl = new ContractControl(contract, playerName, clubName, agentName); // передати імена гравця, клубу та агента до ContractControl
                        ContractsTableFlowPanel.Controls.Add(contractControl);
                    }
                }
            }
        }

        private void ContractsTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;

            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                if (form.CurrentUser != null && form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))
                    form.ShowContractChangeAddControl();
                else
                {
                    form.ShowError("У вас немає прав на додавання нових контрактів!");
                }
            }

        }
    }
}
