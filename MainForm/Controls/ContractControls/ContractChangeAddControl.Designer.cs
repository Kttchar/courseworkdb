﻿namespace MainForm.Controls.ContractControls
{
    partial class ContractChangeAddControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartDatePicker = new System.Windows.Forms.DateTimePicker();
            this.AgentPictureBox = new System.Windows.Forms.PictureBox();
            this.ClubPictureBox = new System.Windows.Forms.PictureBox();
            this.ContractStatusComboBox = new System.Windows.Forms.ComboBox();
            this.ContractStatusLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.StartDateLabel = new System.Windows.Forms.Label();
            this.SalaryLabel = new System.Windows.Forms.Label();
            this.SalaryTextBox = new System.Windows.Forms.TextBox();
            this.AgentNameLabel = new System.Windows.Forms.Label();
            this.AgentNameTextBox = new System.Windows.Forms.TextBox();
            this.ClubNameLabel = new System.Windows.Forms.Label();
            this.ClubNameTextBox = new System.Windows.Forms.TextBox();
            this.PlayerPictureBox = new System.Windows.Forms.PictureBox();
            this.PlayerSecondNameLabel = new System.Windows.Forms.Label();
            this.PlayerNameLabel = new System.Windows.Forms.Label();
            this.PlayerSecondNameTextBox = new System.Windows.Forms.TextBox();
            this.PlayerNameTextBox = new System.Windows.Forms.TextBox();
            this.ChangeAddButton = new System.Windows.Forms.Button();
            this.EndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.EndDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AgentPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClubPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // StartDatePicker
            // 
            this.StartDatePicker.Enabled = false;
            this.StartDatePicker.Location = new System.Drawing.Point(260, 211);
            this.StartDatePicker.Name = "StartDatePicker";
            this.StartDatePicker.Size = new System.Drawing.Size(136, 20);
            this.StartDatePicker.TabIndex = 109;
            // 
            // AgentPictureBox
            // 
            this.AgentPictureBox.Location = new System.Drawing.Point(395, 84);
            this.AgentPictureBox.Name = "AgentPictureBox";
            this.AgentPictureBox.Size = new System.Drawing.Size(81, 57);
            this.AgentPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AgentPictureBox.TabIndex = 108;
            this.AgentPictureBox.TabStop = false;
            // 
            // ClubPictureBox
            // 
            this.ClubPictureBox.Location = new System.Drawing.Point(127, 84);
            this.ClubPictureBox.Name = "ClubPictureBox";
            this.ClubPictureBox.Size = new System.Drawing.Size(81, 57);
            this.ClubPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ClubPictureBox.TabIndex = 107;
            this.ClubPictureBox.TabStop = false;
            // 
            // ContractStatusComboBox
            // 
            this.ContractStatusComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ContractStatusComboBox.FormattingEnabled = true;
            this.ContractStatusComboBox.Items.AddRange(new object[] {
            "активний",
            "неактивний",
            "очікує підтвердження"});
            this.ContractStatusComboBox.Location = new System.Drawing.Point(276, 285);
            this.ContractStatusComboBox.Name = "ContractStatusComboBox";
            this.ContractStatusComboBox.Size = new System.Drawing.Size(121, 21);
            this.ContractStatusComboBox.TabIndex = 106;
            // 
            // ContractStatusLabel
            // 
            this.ContractStatusLabel.AutoSize = true;
            this.ContractStatusLabel.Location = new System.Drawing.Point(137, 288);
            this.ContractStatusLabel.Name = "ContractStatusLabel";
            this.ContractStatusLabel.Size = new System.Drawing.Size(103, 13);
            this.ContractStatusLabel.TabIndex = 105;
            this.ContractStatusLabel.Text = "ContractStatusLabel";
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(492, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(37, 18);
            this.CloseButton.TabIndex = 104;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // StartDateLabel
            // 
            this.StartDateLabel.AutoSize = true;
            this.StartDateLabel.Location = new System.Drawing.Point(121, 211);
            this.StartDateLabel.Name = "StartDateLabel";
            this.StartDateLabel.Size = new System.Drawing.Size(78, 13);
            this.StartDateLabel.TabIndex = 103;
            this.StartDateLabel.Text = "StartDateLabel";
            // 
            // SalaryLabel
            // 
            this.SalaryLabel.AutoSize = true;
            this.SalaryLabel.Location = new System.Drawing.Point(151, 188);
            this.SalaryLabel.Name = "SalaryLabel";
            this.SalaryLabel.Size = new System.Drawing.Size(62, 13);
            this.SalaryLabel.TabIndex = 102;
            this.SalaryLabel.Text = "SalaryLabel";
            // 
            // SalaryTextBox
            // 
            this.SalaryTextBox.Location = new System.Drawing.Point(285, 185);
            this.SalaryTextBox.Name = "SalaryTextBox";
            this.SalaryTextBox.Size = new System.Drawing.Size(121, 20);
            this.SalaryTextBox.TabIndex = 101;
            // 
            // AgentNameLabel
            // 
            this.AgentNameLabel.AutoSize = true;
            this.AgentNameLabel.Location = new System.Drawing.Point(273, 151);
            this.AgentNameLabel.Name = "AgentNameLabel";
            this.AgentNameLabel.Size = new System.Drawing.Size(89, 13);
            this.AgentNameLabel.TabIndex = 100;
            this.AgentNameLabel.Text = "AgentNameLabel";
            // 
            // AgentNameTextBox
            // 
            this.AgentNameTextBox.Location = new System.Drawing.Point(395, 148);
            this.AgentNameTextBox.Name = "AgentNameTextBox";
            this.AgentNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.AgentNameTextBox.TabIndex = 99;
            // 
            // ClubNameLabel
            // 
            this.ClubNameLabel.AutoSize = true;
            this.ClubNameLabel.Location = new System.Drawing.Point(5, 147);
            this.ClubNameLabel.Name = "ClubNameLabel";
            this.ClubNameLabel.Size = new System.Drawing.Size(82, 13);
            this.ClubNameLabel.TabIndex = 98;
            this.ClubNameLabel.Text = "ClubNameLabel";
            // 
            // ClubNameTextBox
            // 
            this.ClubNameTextBox.Location = new System.Drawing.Point(127, 144);
            this.ClubNameTextBox.Name = "ClubNameTextBox";
            this.ClubNameTextBox.ReadOnly = true;
            this.ClubNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.ClubNameTextBox.TabIndex = 97;
            // 
            // PlayerPictureBox
            // 
            this.PlayerPictureBox.Location = new System.Drawing.Point(315, 17);
            this.PlayerPictureBox.Name = "PlayerPictureBox";
            this.PlayerPictureBox.Size = new System.Drawing.Size(81, 57);
            this.PlayerPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PlayerPictureBox.TabIndex = 96;
            this.PlayerPictureBox.TabStop = false;
            // 
            // PlayerSecondNameLabel
            // 
            this.PlayerSecondNameLabel.AutoSize = true;
            this.PlayerSecondNameLabel.Location = new System.Drawing.Point(38, 57);
            this.PlayerSecondNameLabel.Name = "PlayerSecondNameLabel";
            this.PlayerSecondNameLabel.Size = new System.Drawing.Size(127, 13);
            this.PlayerSecondNameLabel.TabIndex = 95;
            this.PlayerSecondNameLabel.Text = "PlayerSecondNameLabel";
            // 
            // PlayerNameLabel
            // 
            this.PlayerNameLabel.AutoSize = true;
            this.PlayerNameLabel.Location = new System.Drawing.Point(41, 25);
            this.PlayerNameLabel.Name = "PlayerNameLabel";
            this.PlayerNameLabel.Size = new System.Drawing.Size(90, 13);
            this.PlayerNameLabel.TabIndex = 94;
            this.PlayerNameLabel.Text = "PlayerNameLabel";
            // 
            // PlayerSecondNameTextBox
            // 
            this.PlayerSecondNameTextBox.Location = new System.Drawing.Point(171, 54);
            this.PlayerSecondNameTextBox.Name = "PlayerSecondNameTextBox";
            this.PlayerSecondNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.PlayerSecondNameTextBox.TabIndex = 93;
            // 
            // PlayerNameTextBox
            // 
            this.PlayerNameTextBox.Location = new System.Drawing.Point(171, 22);
            this.PlayerNameTextBox.Name = "PlayerNameTextBox";
            this.PlayerNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.PlayerNameTextBox.TabIndex = 92;
            // 
            // ChangeAddButton
            // 
            this.ChangeAddButton.Location = new System.Drawing.Point(171, 312);
            this.ChangeAddButton.Name = "ChangeAddButton";
            this.ChangeAddButton.Size = new System.Drawing.Size(161, 23);
            this.ChangeAddButton.TabIndex = 91;
            this.ChangeAddButton.Text = "Додати користувача";
            this.ChangeAddButton.UseVisualStyleBackColor = true;
            this.ChangeAddButton.Click += new System.EventHandler(this.ChangeAddButton_Click);
            // 
            // EndDatePicker
            // 
            this.EndDatePicker.Location = new System.Drawing.Point(261, 248);
            this.EndDatePicker.Name = "EndDatePicker";
            this.EndDatePicker.Size = new System.Drawing.Size(136, 20);
            this.EndDatePicker.TabIndex = 111;
            // 
            // EndDateLabel
            // 
            this.EndDateLabel.AutoSize = true;
            this.EndDateLabel.Location = new System.Drawing.Point(122, 248);
            this.EndDateLabel.Name = "EndDateLabel";
            this.EndDateLabel.Size = new System.Drawing.Size(75, 13);
            this.EndDateLabel.TabIndex = 110;
            this.EndDateLabel.Text = "EndDateLabel";
            // 
            // ContractChangeAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.EndDatePicker);
            this.Controls.Add(this.EndDateLabel);
            this.Controls.Add(this.StartDatePicker);
            this.Controls.Add(this.AgentPictureBox);
            this.Controls.Add(this.ClubPictureBox);
            this.Controls.Add(this.ContractStatusComboBox);
            this.Controls.Add(this.ContractStatusLabel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.StartDateLabel);
            this.Controls.Add(this.SalaryLabel);
            this.Controls.Add(this.SalaryTextBox);
            this.Controls.Add(this.AgentNameLabel);
            this.Controls.Add(this.AgentNameTextBox);
            this.Controls.Add(this.ClubNameLabel);
            this.Controls.Add(this.ClubNameTextBox);
            this.Controls.Add(this.PlayerPictureBox);
            this.Controls.Add(this.PlayerSecondNameLabel);
            this.Controls.Add(this.PlayerNameLabel);
            this.Controls.Add(this.PlayerSecondNameTextBox);
            this.Controls.Add(this.PlayerNameTextBox);
            this.Controls.Add(this.ChangeAddButton);
            this.Name = "ContractChangeAddControl";
            this.Size = new System.Drawing.Size(529, 346);
            this.Load += new System.EventHandler(this.ContractChangeAddControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AgentPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClubPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker StartDatePicker;
        private System.Windows.Forms.PictureBox AgentPictureBox;
        private System.Windows.Forms.PictureBox ClubPictureBox;
        private System.Windows.Forms.ComboBox ContractStatusComboBox;
        private System.Windows.Forms.Label ContractStatusLabel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label StartDateLabel;
        private System.Windows.Forms.Label SalaryLabel;
        private System.Windows.Forms.TextBox SalaryTextBox;
        private System.Windows.Forms.Label AgentNameLabel;
        private System.Windows.Forms.TextBox AgentNameTextBox;
        private System.Windows.Forms.Label ClubNameLabel;
        private System.Windows.Forms.TextBox ClubNameTextBox;
        private System.Windows.Forms.PictureBox PlayerPictureBox;
        private System.Windows.Forms.Label PlayerSecondNameLabel;
        private System.Windows.Forms.Label PlayerNameLabel;
        private System.Windows.Forms.TextBox PlayerSecondNameTextBox;
        private System.Windows.Forms.TextBox PlayerNameTextBox;
        private System.Windows.Forms.Button ChangeAddButton;
        private System.Windows.Forms.DateTimePicker EndDatePicker;
        private System.Windows.Forms.Label EndDateLabel;
    }
}
