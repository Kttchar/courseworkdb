﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FC_Library;

namespace MainForm.Controls.ContractControls
{
    public partial class ContractChangeAddControl : UserControl
    {
        private Contract _contract;

        public ContractChangeAddControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;

            // Зміна тексту лейбелів та кнопки
            PlayerNameLabel.Text = "Ім'я гравця:";
            PlayerSecondNameLabel.Text = "Прізвище гравця:";
            ClubNameLabel.Text = "Назва клубу:";
            AgentNameLabel.Text = "Ім'я агента:";
            StartDateLabel.Text = "Дата початку:";
            EndDateLabel.Text = "Дата закінчення:";
            SalaryLabel.Text = "Зарплата:";
            ContractStatusLabel.Text = "Статус контракту:";
            ChangeAddButton.Text = "Додати контракт";
            StartDatePicker.Value = DateTime.UtcNow;
        }

        public ContractChangeAddControl(Contract contract) : this()
        {
            _contract = contract;

            // Змінюємо текст лейбелів та кнопки
            ChangeAddButton.Text = "Змінити дані контракту";

            // Вставляємо дані контракту в текстбокси
            SalaryTextBox.Text = ((decimal)_contract.Salary).ToString("F0");

            StartDatePicker.Value = _contract.StartDate;
            EndDatePicker.Value = _contract.EndDate;

            ContractStatusComboBox.SelectedItem = _contract.ContractStatus;
        }

        private async void ChangeAddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Перевірка при додаванні нового контракту
                if (_contract == null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    form.ShowError("У вас немає прав на додавання нових контрактів!");
                    return;
                }

                // Перевірка при редагуванні контракту
                if (_contract != null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "administrator" && _contract.ContractStatus == "активний" ||
                (form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && (c.ClubID == _contract.ClubID)) ||
                (form.CurrentUser.UserType == "player" && _contract.ContractStatus == "очікує підтвердження" && context.Players.Any(p => p.UserPublicID == form.CurrentUser.UserPublicID && p.PlayerID == _contract.PlayerID)
                )))))
                {
                    form.ShowError("У вас немає прав на редагування цього контракту!");
                    return;
                }

                // Перевірка при додаванні нового контракту
                if (_contract == null && form.CurrentUser != null && form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))
                {
                    // Клубський акаунт клуба при створенні контракту може встановити статус тільки "очікує підтвердження"
                    ContractStatusComboBox.SelectedItem = "очікує підтвердження";
                }

                // Перевірка при редагуванні контракту
                if (_contract != null && form.CurrentUser != null && form.CurrentUser.UserType == "player" && context.Players.Any(p => p.UserPublicID == form.CurrentUser.UserPublicID && p.PlayerID == _contract.PlayerID))
                {
                    // Гравець, який бере участь у контракті, може змінити - це встановити статус "активний"
                    if (ContractStatusComboBox.SelectedItem.ToString() != "активний")
                    {
                        form.ShowError("Ви можете  змінити статус контракту тільки на 'активний'!");
                        return;
                    }
                }


                // Перевірка при редагуванні контракту
                if (_contract != null && form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" || (form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && c.ClubID == _contract.ClubID))))
                {
                    // Адміністратор та клубський акаунт що прив'язаний до клубу можуть змінити - це встановити статус "неактивний"
                    if (ContractStatusComboBox.SelectedItem.ToString() != "неактивний")
                    {
                        form.ShowError("Ви можете тільки змінити статус контракту на 'неактивний'!");
                        return;
                    }
                }

                // Перевірка, чи всі поля заповнені
                if (string.IsNullOrEmpty(PlayerNameTextBox.Text) ||
                    string.IsNullOrEmpty(PlayerSecondNameTextBox.Text) ||
                    string.IsNullOrEmpty(ClubNameTextBox.Text) ||
                    string.IsNullOrEmpty(AgentNameTextBox.Text) ||
                    string.IsNullOrEmpty(SalaryTextBox.Text) ||
                    string.IsNullOrEmpty(ContractStatusComboBox.Text))
                {
                    form.ShowError("Будь ласка, заповніть всі поля!");
                    return;
                }

                // Перевірка, чи існує гравець з таким ім'ям
                var existingPlayer = await context.Players.FirstOrDefaultAsync(p => p.PlayerName == PlayerNameTextBox.Text && p.PlayerSecondName == PlayerSecondNameTextBox.Text);
                if (existingPlayer == null)
                {
                    form.ShowError("Гравця з таким ім'ям та прізвищем не існує!");
                    return;
                }

                // Перевірка, чи існує клуб з таким ім'ям
                var existingClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubName == ClubNameTextBox.Text);
                if (existingClub == null)
                {
                    form.ShowError("Клубу з таким ім'ям не існує!");
                    return;
                }

                // Перевірка, чи існує агент з таким ім'ям
                var existingAgent = await context.Agents.FirstOrDefaultAsync(a => a.AgentName == AgentNameTextBox.Text);
                if (existingAgent == null)
                {
                    form.ShowError("Агента з таким ім'ям не існує!");
                    return;
                }

                // Перевірка, чи гравець належить клубу "Гравець без клуба"
                var existingPlayerClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubID == existingPlayer.ClubID);
                if (existingPlayerClub.ClubName != "Гравець без клубу" && _contract.ContractStatus!="очікує підтвердження")
                {
                    form.ShowError("Контракт можна укласти тільки з гравцем який не є учасником іншого клубу!");
                    return;
                }

                // Перевірка, чи дата закінчення контракту є більшою за поточну дату на рік
                if (EndDatePicker.Value <= DateTime.UtcNow.AddYears(1))
                {
                    form.ShowError("Дата закінчення контракту повинна бути як мінімум на рік більша за поточну дату!");
                    return;
                }

                string salaryText = SalaryTextBox.Text.Replace(',', '.');
                if (!double.TryParse(salaryText, NumberStyles.Float, CultureInfo.InvariantCulture, out double salary))
                {
                    form.ShowError("Недійсне значення зарплати!");
                    return;
                }

                if(ContractStatusComboBox.Text == "активний" || ContractStatusComboBox.Text == "очікує підтвердження") {

                    StartDatePicker.Value = DateTime.UtcNow;
                }
                else if (ContractStatusComboBox.Text == "неактивний")
                {
                    EndDatePicker.Value = DateTime.UtcNow;
                }
                
                var parameters = new[]
                {
                    new SqlParameter("@playerName", existingPlayer.PlayerName),
                    new SqlParameter("@playerSecondName", existingPlayer.PlayerSecondName),
                    new SqlParameter("@clubName", existingClub.ClubName),
                    new SqlParameter("@agentName", existingAgent.AgentName),
                    new SqlParameter("@startDate", StartDatePicker.Value.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                    new SqlParameter("@endDate", EndDatePicker.Value.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                    new SqlParameter("@salary", SqlDbType.Float) { Value = salary },
                    new SqlParameter("@contractStatus", ContractStatusComboBox.Text)
                };

                if (_contract == null)
                {
                    // Виклик процедури AddContract
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC AddContract @playerName, @playerSecondName, @clubName, @agentName, @startDate, @endDate, @salary, @contractStatus",
                        parameters: parameters
                    );
                    form.ShowInfo("Контракт успішно доданий!");
                }
                else
                {
                    // Виклик процедури ChangeContract
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC ChangeContract @contractID, @playerName, @playerSecondName, @clubName, @agentName, @startDate, @endDate, @salary, @contractStatus",
                        parameters: parameters.Prepend(new SqlParameter("@contractID", _contract.ContractID)).ToArray()
                    );
                    form.ShowInfo("Дані контракту успішно змінено!");
                }

                this.Visible = false;
            }
        }

        private async void ContractChangeAddControl_Load(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Перевірка при додаванні нового контракту
                if (_contract == null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    ContractStatusComboBox.Enabled = false;
                    ChangeAddButton.Enabled = false;
                    ChangeAddButton.Visible = false;
                }

                // Перевірка при редагуванні контракту
                if (_contract != null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "administrator" ||
                (form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && (c.ClubID == _contract.ClubID)) ||
                (form.CurrentUser.UserType == "player" && _contract.ContractStatus=="очікує підтвердження" && context.Players.Any(p => p.UserPublicID == form.CurrentUser.UserPublicID && p.PlayerID == _contract.PlayerID)
                )))))
                {

                    ContractStatusComboBox.Enabled = false;
                    ChangeAddButton.Enabled = false;
                    ChangeAddButton.Visible = false;
                }
            }
            if (_contract != null)
            {
                // поля крім статусу - рідонлі з початку Load
                PlayerNameTextBox.ReadOnly = true;
                PlayerSecondNameTextBox.ReadOnly = true;
                ClubNameTextBox.ReadOnly = true;
                AgentNameTextBox.ReadOnly = true;
                SalaryTextBox.ReadOnly = true;

                StartDatePicker.Enabled = false;
                EndDatePicker.Enabled = false;

                // Якщо статус контракту вже "неактивний" - кнопка зміни недоступна
                if (_contract.ContractStatus == "неактивний")
                {
                    ContractStatusComboBox.Enabled = false;
                    ChangeAddButton.Enabled = false;
                    ChangeAddButton.Visible = false;
                }

                try
                {
                    using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                    {
                        var player = await context.Players.FirstOrDefaultAsync(p => p.PlayerID == _contract.PlayerID);
                        var club = await context.Clubs.FirstOrDefaultAsync(c => c.ClubID == _contract.ClubID);
                        var agent = await context.Agents.FirstOrDefaultAsync(a => a.AgentID == _contract.AgentID);

                        if (player != null && club != null && agent != null)
                        {
                            PlayerNameTextBox.Text = player.PlayerName;
                            PlayerSecondNameTextBox.Text = player.PlayerSecondName;
                            ClubNameTextBox.Text = club.ClubName;
                            AgentNameTextBox.Text = agent.AgentName;

                            if (player.Photo != null)
                            {
                                using (var ms = new MemoryStream(player.Photo))
                                {
                                    PlayerPictureBox.Image = Image.FromStream(ms);
                                }
                            }

                            if (club.Logo != null)
                            {
                                using (var ms = new MemoryStream(club.Logo))
                                {
                                    ClubPictureBox.Image = Image.FromStream(ms);
                                }
                            }
                            if (agent.Photo != null)
                            {
                                using (var ms = new MemoryStream(agent.Photo))
                                {
                                    AgentPictureBox.Image = Image.FromStream(ms);
                                }
                            }
                        }
                        else
                        {
                            // Якщо гравець, клуб або агент не знайдено, повідомлення про помилку
                            form.ShowError("Гравця, клубу або агента з таким ID не існує!");
                        }
                    }
                }
                catch (Exception ex) { }
            }
            else if (_contract == null)
            {
                using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                {
                    if (form.CurrentUser != null && form.CurrentUser.UserType == "club")
                    {
                        var club = await context.Clubs.FirstOrDefaultAsync(c => c.UserPublicID == form.CurrentUser.UserPublicID);
                        if (club != null)
                        ClubNameTextBox.Text = club.ClubName;
                    }
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
