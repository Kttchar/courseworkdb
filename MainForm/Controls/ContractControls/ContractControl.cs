﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FC_Library;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ContractControl : UserControl
    {

        private Contract _contract;
        private string _playerName;
        private string _clubName;
        private string _agentName;

        public ContractControl(Contract contract, string playerName, string clubName, string agentName)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _contract = contract;
            _playerName = playerName;
            _clubName = clubName;
            _agentName = agentName;

           



            playerNameLabel.Text = _playerName;
            clubNameLabel.Text = _clubName;
            agentNameLabel.Text = _agentName;
            startDateLabel.Text = _contract.StartDate.ToShortDateString();
            endDateLabel.Text = _contract.EndDate.ToShortDateString();
            salaryLabel.Text = _contract.Salary.ToString();
            contractStatusLabel.Text = _contract.ContractStatus;
            switch (_contract.ContractStatus)
            {
                case "активний":
                    contractStatusLabel.BackColor = Color.LightGreen;
                    break;
                case "неактивний":
                    contractStatusLabel.BackColor = Color.Salmon; // Світліший червоний
                    break;
                case "очікує підтвердження":
                    contractStatusLabel.BackColor = Color.Yellow;
                    break;
                default:
                    contractStatusLabel.BackColor = Color.Gray; // Колір за замовчуванням
                    break;
            }
            contractStatusLabel.Text = _contract.ContractStatus;

        }

        private void ContractControl_DoubleClick(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;

            form.ShowContractChangeAddControl(_contract);
        }
    }
}
