﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ChangeUserDataControl : UserControl
    {
        private byte[] _newAvatar;
        private User _user;
        public ChangeUserDataControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            this.BackColor = Color.WhiteSmoke;
        }
        private async void confirmChangesButton_Click(object sender, EventArgs e)
        {
            string newUsername = newUsernameTextBox.Text;
            string newPassword = newPasswordTextBox.Text;
            string confirmPassword = confirmNewPasswordTextBox.Text;
            string currentPassword = currentPasswordTextBox.Text;


            var form = this.Parent as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();


            if (newPassword != confirmPassword)
            {
                // Показати повідомлення про помилку
                form.ShowError("Новий пароль та підтвердження пароля не співпадають.");
                return;
            }

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                if (!string.IsNullOrEmpty(newUsername))
                {
                    var existingUser = await context.Users.SingleOrDefaultAsync(u => u.Username == newUsername);
                    if (existingUser != null)
                    {
                        // Показати повідомлення про помилку
                        form.ShowError("Користувач з таким ім'ям вже існує. Будь ласка, виберіть інше ім'я.");
                        return;
                    }

                    var parameters = new[]
                    {
                        new SqlParameter("@userID", _user.UserID),
                        new SqlParameter("@newUsername", newUsername)
                    };

                    await context.Database.ExecuteSqlCommandAsync("EXEC UpdateUsername @userID, @newUsername", parameters);
                    form.ShowInfo($"Ваше ім'я користувача було успішно змінено на {newUsername}.");

                }

                if (!string.IsNullOrEmpty(newPassword))
                {
                    
                    var user = await context.Users.SingleOrDefaultAsync(u => u.UserID == _user.UserID && u.Password == currentPassword);
                    if (user == null)
                    {
                        // Показати повідомлення про помилку
                        form.ShowError("Поточний пароль невірний. Будь ласка, спробуйте ще раз.");
                        return;
                    }

                    var parameters = new[]
                    {
                        new SqlParameter("@userID", _user.UserID),
                        new SqlParameter("@newPassword", newPassword)
                    };

                    await context.Database.ExecuteSqlCommandAsync("EXEC UpdatePassword @userID, @newPassword", parameters);
                    form.ShowInfo("Ваш пароль було успішно оновлено.");
                }

                if (_newAvatar != null)
                {
                    var parameters = new[]
                    {
                        new SqlParameter("@userID", _user.UserID),
                        new SqlParameter("@newAvatar", _newAvatar)
                    };

                    await context.Database.ExecuteSqlCommandAsync("EXEC UpdateAvatar @userID, @newAvatar", parameters);
                    form.ShowInfo("Ваш аватар було успішно оновлено.");
                }
            }

        }


        private void selectPhotoButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Image Files( *.gif; *.bmp; *.png)|*.gif; *.bmp; *.png";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (var stream = new FileStream(ofd.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = new BinaryReader(stream))
                        {
                            _newAvatar = reader.ReadBytes((int)stream.Length);
                        }
                    }

                    avatarPictureBox.Image = Image.FromFile(ofd.FileName);
                }
            }
        }

        public void UpdateUser(User user)
        {
            _user = user;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
