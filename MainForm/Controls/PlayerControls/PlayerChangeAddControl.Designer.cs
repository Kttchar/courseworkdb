﻿namespace MainForm.Controls.PlayerControls
{
    partial class PlayerChangeAddControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseButton = new System.Windows.Forms.Button();
            this.NationalityLabel = new System.Windows.Forms.Label();
            this.NationalityTextBox = new System.Windows.Forms.TextBox();
            this.AgeLabel = new System.Windows.Forms.Label();
            this.AgeTextBox = new System.Windows.Forms.TextBox();
            this.UserPublicIDLabel = new System.Windows.Forms.Label();
            this.UserPublicIDTextBox = new System.Windows.Forms.TextBox();
            this.PhotoPictureBox = new System.Windows.Forms.PictureBox();
            this.selectPhotoButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.PlayerSecondNameLabel = new System.Windows.Forms.Label();
            this.PlayerNameLabel = new System.Windows.Forms.Label();
            this.PlayerSecondNameTextBox = new System.Windows.Forms.TextBox();
            this.PlayerNameTextBox = new System.Windows.Forms.TextBox();
            this.ChangeAddButton = new System.Windows.Forms.Button();
            this.PositionLabel = new System.Windows.Forms.Label();
            this.PositionTextBox = new System.Windows.Forms.TextBox();
            this.ClubNameLabel = new System.Windows.Forms.Label();
            this.ClubNameTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PhotoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(438, 2);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(37, 18);
            this.CloseButton.TabIndex = 61;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // NationalityLabel
            // 
            this.NationalityLabel.AutoSize = true;
            this.NationalityLabel.Location = new System.Drawing.Point(4, 149);
            this.NationalityLabel.Name = "NationalityLabel";
            this.NationalityLabel.Size = new System.Drawing.Size(82, 13);
            this.NationalityLabel.TabIndex = 60;
            this.NationalityLabel.Text = "NationalityLabel";
            // 
            // NationalityTextBox
            // 
            this.NationalityTextBox.Location = new System.Drawing.Point(148, 146);
            this.NationalityTextBox.Name = "NationalityTextBox";
            this.NationalityTextBox.Size = new System.Drawing.Size(121, 20);
            this.NationalityTextBox.TabIndex = 59;
            // 
            // AgeLabel
            // 
            this.AgeLabel.AutoSize = true;
            this.AgeLabel.Location = new System.Drawing.Point(12, 110);
            this.AgeLabel.Name = "AgeLabel";
            this.AgeLabel.Size = new System.Drawing.Size(52, 13);
            this.AgeLabel.TabIndex = 58;
            this.AgeLabel.Text = "AgeLabel";
            // 
            // AgeTextBox
            // 
            this.AgeTextBox.Location = new System.Drawing.Point(148, 107);
            this.AgeTextBox.Name = "AgeTextBox";
            this.AgeTextBox.Size = new System.Drawing.Size(121, 20);
            this.AgeTextBox.TabIndex = 57;
            // 
            // UserPublicIDLabel
            // 
            this.UserPublicIDLabel.AutoSize = true;
            this.UserPublicIDLabel.Location = new System.Drawing.Point(42, 245);
            this.UserPublicIDLabel.Name = "UserPublicIDLabel";
            this.UserPublicIDLabel.Size = new System.Drawing.Size(140, 13);
            this.UserPublicIDLabel.TabIndex = 56;
            this.UserPublicIDLabel.Text = "Публічний ID користувача:";
            // 
            // UserPublicIDTextBox
            // 
            this.UserPublicIDTextBox.Location = new System.Drawing.Point(188, 242);
            this.UserPublicIDTextBox.Name = "UserPublicIDTextBox";
            this.UserPublicIDTextBox.Size = new System.Drawing.Size(121, 20);
            this.UserPublicIDTextBox.TabIndex = 55;
            // 
            // PhotoPictureBox
            // 
            this.PhotoPictureBox.Location = new System.Drawing.Point(295, 52);
            this.PhotoPictureBox.Name = "PhotoPictureBox";
            this.PhotoPictureBox.Size = new System.Drawing.Size(166, 145);
            this.PhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PhotoPictureBox.TabIndex = 54;
            this.PhotoPictureBox.TabStop = false;
            // 
            // selectPhotoButton
            // 
            this.selectPhotoButton.Location = new System.Drawing.Point(309, 203);
            this.selectPhotoButton.Name = "selectPhotoButton";
            this.selectPhotoButton.Size = new System.Drawing.Size(134, 23);
            this.selectPhotoButton.TabIndex = 53;
            this.selectPhotoButton.Text = "Вибрати фото";
            this.selectPhotoButton.UseVisualStyleBackColor = true;
            this.selectPhotoButton.Click += new System.EventHandler(this.selectPhotoButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(351, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 52;
            this.label5.Text = "Фото:";
            // 
            // PlayerSecondNameLabel
            // 
            this.PlayerSecondNameLabel.AutoSize = true;
            this.PlayerSecondNameLabel.Location = new System.Drawing.Point(1, 71);
            this.PlayerSecondNameLabel.Name = "PlayerSecondNameLabel";
            this.PlayerSecondNameLabel.Size = new System.Drawing.Size(127, 13);
            this.PlayerSecondNameLabel.TabIndex = 51;
            this.PlayerSecondNameLabel.Text = "PlayerSecondNameLabel";
            // 
            // PlayerNameLabel
            // 
            this.PlayerNameLabel.AutoSize = true;
            this.PlayerNameLabel.Location = new System.Drawing.Point(4, 39);
            this.PlayerNameLabel.Name = "PlayerNameLabel";
            this.PlayerNameLabel.Size = new System.Drawing.Size(90, 13);
            this.PlayerNameLabel.TabIndex = 50;
            this.PlayerNameLabel.Text = "PlayerNameLabel";
            // 
            // PlayerSecondNameTextBox
            // 
            this.PlayerSecondNameTextBox.Location = new System.Drawing.Point(148, 68);
            this.PlayerSecondNameTextBox.Name = "PlayerSecondNameTextBox";
            this.PlayerSecondNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.PlayerSecondNameTextBox.TabIndex = 49;
            // 
            // PlayerNameTextBox
            // 
            this.PlayerNameTextBox.Location = new System.Drawing.Point(148, 36);
            this.PlayerNameTextBox.Name = "PlayerNameTextBox";
            this.PlayerNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.PlayerNameTextBox.TabIndex = 48;
            // 
            // ChangeAddButton
            // 
            this.ChangeAddButton.Location = new System.Drawing.Point(177, 280);
            this.ChangeAddButton.Name = "ChangeAddButton";
            this.ChangeAddButton.Size = new System.Drawing.Size(161, 23);
            this.ChangeAddButton.TabIndex = 47;
            this.ChangeAddButton.Text = "Додати користувача";
            this.ChangeAddButton.UseVisualStyleBackColor = true;
            this.ChangeAddButton.Click += new System.EventHandler(this.ChangeAddButton_Click);
            // 
            // PositionLabel
            // 
            this.PositionLabel.AutoSize = true;
            this.PositionLabel.Location = new System.Drawing.Point(12, 180);
            this.PositionLabel.Name = "PositionLabel";
            this.PositionLabel.Size = new System.Drawing.Size(70, 13);
            this.PositionLabel.TabIndex = 63;
            this.PositionLabel.Text = "PositionLabel";
            // 
            // PositionTextBox
            // 
            this.PositionTextBox.Location = new System.Drawing.Point(148, 177);
            this.PositionTextBox.Name = "PositionTextBox";
            this.PositionTextBox.Size = new System.Drawing.Size(121, 20);
            this.PositionTextBox.TabIndex = 62;
            // 
            // ClubNameLabel
            // 
            this.ClubNameLabel.AutoSize = true;
            this.ClubNameLabel.Location = new System.Drawing.Point(12, 209);
            this.ClubNameLabel.Name = "ClubNameLabel";
            this.ClubNameLabel.Size = new System.Drawing.Size(82, 13);
            this.ClubNameLabel.TabIndex = 65;
            this.ClubNameLabel.Text = "ClubNameLabel";
            // 
            // ClubNameTextBox
            // 
            this.ClubNameTextBox.Location = new System.Drawing.Point(148, 206);
            this.ClubNameTextBox.Name = "ClubNameTextBox";
            this.ClubNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.ClubNameTextBox.TabIndex = 64;
            // 
            // PlayerChangeAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ClubNameLabel);
            this.Controls.Add(this.ClubNameTextBox);
            this.Controls.Add(this.PositionLabel);
            this.Controls.Add(this.PositionTextBox);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.NationalityLabel);
            this.Controls.Add(this.NationalityTextBox);
            this.Controls.Add(this.AgeLabel);
            this.Controls.Add(this.AgeTextBox);
            this.Controls.Add(this.UserPublicIDLabel);
            this.Controls.Add(this.UserPublicIDTextBox);
            this.Controls.Add(this.PhotoPictureBox);
            this.Controls.Add(this.selectPhotoButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PlayerSecondNameLabel);
            this.Controls.Add(this.PlayerNameLabel);
            this.Controls.Add(this.PlayerSecondNameTextBox);
            this.Controls.Add(this.PlayerNameTextBox);
            this.Controls.Add(this.ChangeAddButton);
            this.Name = "PlayerChangeAddControl";
            this.Size = new System.Drawing.Size(480, 315);
            this.Load += new System.EventHandler(this.PlayerChangeAddControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PhotoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label NationalityLabel;
        private System.Windows.Forms.TextBox NationalityTextBox;
        private System.Windows.Forms.Label AgeLabel;
        private System.Windows.Forms.TextBox AgeTextBox;
        private System.Windows.Forms.Label UserPublicIDLabel;
        private System.Windows.Forms.TextBox UserPublicIDTextBox;
        private System.Windows.Forms.PictureBox PhotoPictureBox;
        private System.Windows.Forms.Button selectPhotoButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label PlayerSecondNameLabel;
        private System.Windows.Forms.Label PlayerNameLabel;
        private System.Windows.Forms.TextBox PlayerSecondNameTextBox;
        private System.Windows.Forms.TextBox PlayerNameTextBox;
        private System.Windows.Forms.Button ChangeAddButton;
        private System.Windows.Forms.Label PositionLabel;
        private System.Windows.Forms.TextBox PositionTextBox;
        private System.Windows.Forms.Label ClubNameLabel;
        private System.Windows.Forms.TextBox ClubNameTextBox;
    }
}
