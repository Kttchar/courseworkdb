﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm.Controls.PlayerControls
{
    public partial class PlayerChangeAddControl : UserControl
    {

        private Player _player;

        public PlayerChangeAddControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            // Зміна тексту лейбелів та кнопки
            PlayerNameLabel.Text = "Нове ім'я:";
            PlayerSecondNameLabel.Text = "Нове прізвище:";
            AgeLabel.Text = "Новий вік:";
            NationalityLabel.Text = "Нова національність:";
            PositionLabel.Text = "Нова позиція:";
            ClubNameLabel.Text = "Новий клуб:";
            ChangeAddButton.Text = "Додати гравця";
        }
        public PlayerChangeAddControl(Player player) : this()
        {
            _player = player;
            // Змінюємо текст лейбелів та кнопки
            PlayerNameLabel.Text = "Змінити ім'я:";
            PlayerSecondNameLabel.Text = "Змінити прізвище:";
            AgeLabel.Text = "Змінити вік:";
            NationalityLabel.Text = "Змінити національність:";
            PositionLabel.Text = "Змінити позицію:";
            ClubNameLabel.Text = "Змінити клуб на:";
            ChangeAddButton.Text = "Змінити дані гравця";
            // Вставляємо дані гравця в текстбокси
            PlayerNameTextBox.Text = _player.PlayerName;
            PlayerSecondNameTextBox.Text = _player.PlayerSecondName;
            AgeTextBox.Text = _player.Age.ToString();
            NationalityTextBox.Text = _player.Nationality;
            PositionTextBox.Text = _player.Position;
            UserPublicIDTextBox.Text = _player.UserPublicID;

            


                if (_player.Photo != null)
            {
                using (var ms = new MemoryStream(_player.Photo))
                {
                    PhotoPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private async void ChangeAddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Перевірка при додаванні нового гравця
                if (_player == null && form.CurrentUser != null && !(form.CurrentUser.UserType == "administrator" || !(form.CurrentUser.UserType != "administrator" &&
                    (form.CurrentUser.UserType != "player" ||
                     (form.CurrentUser.UserType == "player" && context.Players.Any(p => p.UserPublicID == form.CurrentUser.UserPublicID))))))
                {
                    form.ShowError("У вас немає прав на додавання нових гравців!");
                    return;
                }

                // Перевірка при редагуванні гравця
                if (_player != null && form.CurrentUser != null && !(form.CurrentUser.UserType == "administrator" ||
                     (_player != null && form.CurrentUser.UserType == "player" && form.CurrentUser.UserPublicID == _player.UserPublicID) ||
                     (form.CurrentUser.UserType == "club" && _player != null && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && c.ClubID == _player.ClubID))))
                {
                    form.ShowError("У вас немає прав на редагування цього гравця!");
                    return;
                }

                // Перевірка, чи всі поля заповнені
                if (string.IsNullOrEmpty(PlayerNameTextBox.Text) ||
                string.IsNullOrEmpty(PlayerSecondNameTextBox.Text) ||
                string.IsNullOrEmpty(AgeTextBox.Text) ||
                string.IsNullOrEmpty(NationalityTextBox.Text) ||
                string.IsNullOrEmpty(PositionTextBox.Text) ||
                string.IsNullOrEmpty(ClubNameTextBox.Text)||
                string.IsNullOrEmpty(UserPublicIDTextBox.Text))
                {
                    form.ShowError("Будь ласка, заповніть всі поля!");
                    return;
                }

                byte[] photoBytes = null;
                if (PhotoPictureBox.Image != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        PhotoPictureBox.Image.Save(ms, PhotoPictureBox.Image.RawFormat);
                        photoBytes = ms.ToArray();
                    }
                }

                // Перевірка, чи існує користувач з таким публічним айді
                var existingUser = await context.Users.FirstOrDefaultAsync(u => u.UserPublicID == UserPublicIDTextBox.Text);
                if (existingUser == null)
                {
                    form.ShowError("Користувача з таким публічним ID не існує!");
                    return;
                }

                // Перевірка, чи існує гравець з таким ім'ям
                var existingPlayer = await context.Players.FirstOrDefaultAsync(p => p.PlayerName == PlayerNameTextBox.Text);
                if (existingPlayer != null && (_player == null || existingPlayer.PlayerID != _player.PlayerID))
                {
                    form.ShowError("Гравець з таким ім'ям вже існує!");
                    return;
                }

                // Перевірка, чи існує клуб з таким ім'ям
                var existingClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubName == ClubNameTextBox.Text);
                if (existingClub == null)
                {
                    form.ShowError("Клубу з таким ім'ям не існує!");
                    return;
                }


                if(!uint.TryParse(AgeTextBox.Text, out uint i) || i != int.Parse(AgeTextBox.Text))
                {
                    form.ShowError("Неможливе значення віку!");
                    return;
                }

                if (_player == null)
                {
                    // Виклик процедури AddPlayerByUserID
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC AddPlayerByUserID @p0, @p1, @p2, @p3, @p4, @p5, @p6",
                        PlayerNameTextBox.Text,
                        PlayerSecondNameTextBox.Text,
                        uint.Parse(AgeTextBox.Text),
                        NationalityTextBox.Text,
                        PositionTextBox.Text,
                        photoBytes,
                        UserPublicIDTextBox.Text,
                        ClubNameTextBox.Text
                    );
                    form.ShowInfo("Гравець успішно доданий!");
                }
                else
                {
                    // Виклик процедури ChangePlayerData
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC ChangePlayerData @p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8",
                        _player.PlayerID,
                        PlayerNameTextBox.Text,
                        PlayerSecondNameTextBox.Text,
                        int.Parse(AgeTextBox.Text),
                        NationalityTextBox.Text,
                        PositionTextBox.Text,
                        photoBytes,
                        UserPublicIDTextBox.Text,
                        ClubNameTextBox.Text
                    );
                    form.ShowInfo("Дані гравця успішно змінено!");
                }
            }
        }

        private void selectPhotoButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Image Files( *.gif; *.bmp; *.png)|*.gif; *.bmp; *.png";
                if (ofd.ShowDialog() == DialogResult.OK)
                {


                    PhotoPictureBox.Image = Image.FromFile(ofd.FileName);
                }
            }
        }

        private void PlayerChangeAddControl_Load(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && _player == null)
                UserPublicIDTextBox.Text = form.CurrentUser.UserPublicID;


            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            try
            {
                using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                {
                    var club = context.Clubs.FirstOrDefault(c => c.ClubID == _player.ClubID);
                    if (club != null)
                    {

                        ClubNameTextBox.Text = club.ClubName;
                    }
                    else
                    {
                        // Якщо клуб не знайдено, повідомлення про помилку
                        form.ShowError("Клубу з таким ID не існує!");
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
