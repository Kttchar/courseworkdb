﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class PlayersTableControl : UserControl
    {
        public PlayersTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Отримати всіх гравців з таблиці гравців
                var players = context.Players.ToList();

                // Очистити FlowLayoutPanel
                PlayersTableFlowPanel.Controls.Clear();

                // Створити новий PlayerControl для кожного гравця і додати його до FlowLayoutPanel
                string searchText = SearchTextBox.Text.ToLower();

                foreach (var player in players)
                {
                    // Отримати клуб гравця за ClubID
                    var club = context.Clubs.FirstOrDefault(c => c.ClubID == player.ClubID);
                    string clubName = club != null ? club.ClubName : "";

                    if (string.IsNullOrEmpty(searchText) ||
                        player.PlayerName.ToLower().Contains(searchText) ||
                        player.PlayerSecondName.ToLower().Contains(searchText) ||
                        player.Nationality.ToLower().Contains(searchText) ||
                        player.Position.ToLower().Contains(searchText) ||
                        clubName.ToLower().Contains(searchText)) // додати клуб до фільтру
                    {
                        PlayerControl playerControl = new PlayerControl(player, clubName); // передати ім'я клубу до PlayerControl
                        PlayersTableFlowPanel.Controls.Add(playerControl);
                    }
                }
            }
        }



        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void PlayersTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                if (form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" || !(form.CurrentUser.UserType != "administrator"  &&
                    (form.CurrentUser.UserType != "player" ||
                     (form.CurrentUser.UserType == "player" && context.Players.Any(p => p.UserPublicID == form.CurrentUser.UserPublicID))))))
                {
                    form.ShowPlayerChangeAddControl();
                }
                else
                {
                    form.ShowError("У вас немає прав на додавання гравців!");
                }
            }
        }
    }
}
