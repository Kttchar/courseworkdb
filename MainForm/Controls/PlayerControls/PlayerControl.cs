﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class PlayerControl : UserControl
    {

        private Player _player;
        private string _clubName;

        public PlayerControl(Player player, string clubName)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _player = player;
            _clubName = clubName;

            playerNameLabel.Text = _player.PlayerName;
            playerSecondNameLabel.Text = _player.PlayerSecondName;
            ageLabel.Text = _player.Age.ToString();
            nationalityLabel.Text = _player.Nationality;
            positionLabel.Text = _player.Position;
            clubNameLabel.Text = _clubName; // відображення імені клубу
            if (_clubName == "Гравець без клубу")
                clubNameLabel.BackColor = Color.Salmon;
            else
            {
                clubNameLabel.BackColor = Color.LightGreen;
            }

            if (_player.Photo != null)
            {
                using (var ms = new MemoryStream(_player.Photo))
                {
                    photoPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private void PlayerControl_DoubleClick(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // якщо адміністратор, чи акаунт гравця прив'язаний ло гравця, чи клуб до якого належить гравець
                if (form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" ||
                     (_player != null && form.CurrentUser.UserType == "player" && form.CurrentUser.UserPublicID == _player.UserPublicID) ||
                     (form.CurrentUser.UserType == "club" && _player != null && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && c.ClubID == _player.ClubID))))
                {
                    form.ShowPlayerChangeAddControl(_player);
                }
            }
        }
    }
}
