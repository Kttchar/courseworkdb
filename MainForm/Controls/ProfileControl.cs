﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ProfileControl : UserControl
    {
        public event EventHandler ChangeUserDataButtonClicked;
        public event EventHandler ExitAccountButton_Clicked;
        public ProfileControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            this.BackColor = Color.WhiteSmoke;

            ChangeUserDataButton.Click += ChangeUserDataButton_Click;
        }

        public void DisplayUser(User user)
        {
            usernameLabel.Text = user.Username;
            userTypeLabel.Text = user.UserType;
            userPublicIDLabel.Text = user.UserPublicID;

            if (user.Avatar != null)
            {
                using (var ms = new MemoryStream(user.Avatar))
                {
                    avatarPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        public void ChangeUserDataButton_Click(object sender, EventArgs e)
        {
            ChangeUserDataButtonClicked?.Invoke(this, EventArgs.Empty);
        }

        private void ExitAccountButton_Click(object sender, EventArgs e)
        {
            var form = this.Parent as MainFormFootballDB;
            form.SetCurrentUser();
        }
    }
}
