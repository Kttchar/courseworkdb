﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using MainForm;
using System.Runtime.Remoting.Contexts;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace FC_Library
{
    public partial class LoginControl : UserControl
    {
        private FootballDbContext _context;
        public event Action<User> UserLoggedIn;
        public LoginControl(FootballDbContext context)
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            this.BackColor = Color.WhiteSmoke;
            _context = context;
        }

 

        private async void loginButton_Click(object sender, EventArgs e)
        {
            var form = this.Parent as MainFormFootballDB; // посилання на батьківську форму

            if (string.IsNullOrEmpty(usernameTextBox.Text) ||
                string.IsNullOrEmpty(passwordTextBox.Text) )
            {
                form.ShowError("Будь ласка, заповніть всі поля!");
                return;
            }

            string username = usernameTextBox.Text;
            string password = passwordTextBox.Text;

            
            

            try
            {
                User user;
                var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();
                using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                {
                    {
                        user = await context.Database.SqlQuery<User>("LoginUser @p0, @p1", username, passwordTextBox.Text).SingleOrDefaultAsync();
                    }

                    if (user != null)
                    {
                        // Користувач знайдений, виконати вхід
                        UserLoggedIn?.Invoke(user); // Викличте подію
                    }
                    else
                    {
                        // Користувач не знайдений, показати повідомлення про помилку
                        form.ShowError("Неправильний логін або пароль!\n Спробуйте ще раз.");
                    }
                }
            }
            catch (Exception ex)
            {
                form.ShowError(ex.Message + "Неправильний логін або пароль!\n Спробуйте ще раз.");
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private async void registrateButton_Click(object sender, EventArgs e)
        {
            var form = this.Parent as MainFormFootballDB; // посилання на батьківську форму

            if (string.IsNullOrEmpty(usernameTextBox.Text) ||
                string.IsNullOrEmpty(passwordTextBox.Text))
            {
                form.ShowError("Будь ласка, заповніть всі поля!");
                return;
            }

            string username = usernameTextBox.Text;
            string password = passwordTextBox.Text;

            

            try
            {
                User user;
                var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();
                using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                {
                    // Перевірка, чи вже існує користувач з таким ім'ям користувача
                    user = await context.Users.FirstOrDefaultAsync(u => u.Username == username);
                    if (user != null)
                    {
                        // Користувач з таким ім'ям користувача вже існує, показати повідомлення про помилку
                        form.ShowError("Користувач з таким ім'ям вже існує!\n Спробуйте інше ім'я.");
                        return;
                    }

                    // Виклик процедури AddUserWithBytePhoto
                    // Виклик процедури AddUserWithBytePhoto
                    await context.Database.ExecuteSqlCommandAsync("AddUserWithBytePhoto @p0, @p1, @p2, @p3",
                        new SqlParameter("@p0", username),
                        new SqlParameter("@p1", passwordTextBox.Text),
                        new SqlParameter("@p2", "regular_user"),
                        new SqlParameter("@p3", SqlDbType.VarBinary) { Value = DBNull.Value });

                    form.ShowInfo("Користувач успішно зареєстрований!");
                }
            }
            catch (Exception ex)
            {
                form.ShowError(ex.Message + "\n Не вдалося зареєструвати користувача. Спробуйте ще раз.");
            }
        }
    }
}
