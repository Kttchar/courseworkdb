﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm.Controls.UserControls
{
    public partial class UserDBChangeAddControl : UserControl
    {
        private User _user;

        public UserDBChangeAddControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            // Приховуємо лейбл і текстбокс для публічного айді користувача
            UserPublicIDLabel.Visible = false;
            UserPublicIDTextBox.Visible = false;
            // Змінюємо текст лейбелів та кнопки
            UsernameLabel.Text = "Новий логін:";
            PasswordLabel.Text = "Новий пароль:";
            UserTypeLabel.Text = "Новий статус:";
            ChangeAddButton.Text = "Додати користувача";
        }

        public UserDBChangeAddControl(User user) : this()
        {
            _user = user;
            UserPublicIDLabel.Visible = true;
            UserPublicIDTextBox.Visible = true;
            // Змінюємо текст лейбелів та кнопки
            UsernameLabel.Text = "Змінити логін:";
            PasswordLabel.Text = "Змінити пароль:";
            UserTypeLabel.Text = "Змінити статус:";
            ChangeAddButton.Text = "Підтвердити зміни";
            // Вставляємо дані користувача в текстбокси та комбобокс
            UserPublicIDTextBox.Text = _user.UserPublicID;
            UsernameTextBox.Text = _user.Username;
            PasswordTextBox.Text = _user.Password;
            UserTypeComboBox.SelectedItem = _user.UserType;
            // Вставляємо аватар користувача в пікчурбокс
            if (_user.Avatar != null)
            {
                using (var ms = new MemoryStream(_user.Avatar))
                {
                    AvatarPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private void selectPhotoButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Image Files( *.gif; *.bmp; *.png)|*.gif; *.bmp; *.png";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    

                    AvatarPictureBox.Image = Image.FromFile(ofd.FileName);
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private async void ChangeAddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();
            if (form.CurrentUser == null || form.CurrentUser.UserType != "administrator")
            {
                form.ShowError("У вас немає прав на редагування!");
                return;
            }

            // Перевірка, чи всі поля заповнені
            if (string.IsNullOrEmpty(UsernameTextBox.Text) ||
                string.IsNullOrEmpty(PasswordTextBox.Text) ||
                UserTypeComboBox.SelectedItem == null)
            {
                form.ShowError("Будь ласка, заповніть всі поля!");
                return;
            }

            byte[] avatarBytes = null;
            if (AvatarPictureBox.Image != null)
            {
                using (var ms = new MemoryStream())
                {
                    AvatarPictureBox.Image.Save(ms, AvatarPictureBox.Image.RawFormat);
                    avatarBytes = ms.ToArray();
                }
            }

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Перевірка, чи існує користувач з таким ім'ям
                var existingUser = await context.Users.FirstOrDefaultAsync(u => u.Username == UsernameTextBox.Text);
                if (existingUser != null && (_user == null || existingUser.UserPublicID != _user.UserPublicID))
                {
                    form.ShowError("Користувач з таким ім'ям вже існує!");
                    return;
                }

                string username = UsernameTextBox.Text;
                string password = PasswordTextBox.Text;

                

                if (_user == null)
                {
                    // Виклик процедури AddUser
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC AddUserWithBytePhoto @p0, @p1, @p2, @p3",
                        UsernameTextBox.Text,
                        password,
                        UserTypeComboBox.SelectedItem.ToString(),
                        avatarBytes
                    );
                    form.ShowInfo($"Користувач {UsernameTextBox.Text} успішно доданий!");
                }
                else
                {
                    // Виклик процедури ChangeUserData
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC ChangeUserData @p0, @p1, @p2, @p3, @p4",
                        _user.UserPublicID,
                        UsernameTextBox.Text,
                        password,
                        UserTypeComboBox.SelectedItem.ToString(),
                        avatarBytes
                    );
                    form.ShowInfo($"Дані користувача {_user.UserPublicID} успішно змінено!");
                }
            }
        }
    }
}


