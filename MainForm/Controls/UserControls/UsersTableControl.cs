﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class UsersTableControl : UserControl
    {
        public UsersTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                var users = context.Users.ToList();
                UsersFlowLayoutPanel.Controls.Clear();

                string searchText = SearchTextBox.Text.ToLower();

                foreach (var user in users)
                {
                    var clubs = context.Clubs.Where(c => c.UserPublicID == user.UserPublicID).ToList();
                    var players = context.Players.Where(p => p.UserPublicID == user.UserPublicID).ToList();
                    var agents = context.Agents.Where(a => a.UserPublicID == user.UserPublicID).ToList();

                    if (string.IsNullOrEmpty(searchText) ||
                        user.Username.ToLower().Contains(searchText) ||
                        user.UserType.ToLower().Contains(searchText) ||
                        user.UserPublicID.ToLower().Contains(searchText) ||
                        clubs.Any(c => c.ClubName.ToLower().Contains(searchText)) ||
                        players.Any(p => (p.PlayerName+" "+ p.PlayerSecondName).ToLower().Contains(searchText)) ||
                        agents.Any(a => a.AgentName.ToLower().Contains(searchText)))
                    {
                        UserDBControl userControl = new UserDBControl(user, clubs, players, agents);
                        UsersFlowLayoutPanel.Controls.Add(userControl);
                    }
                }
            }

        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UsersTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && form.CurrentUser.UserType == "administrator")
            {
                form.ShowUserDBChangeAddControl();
            }
            else
            {
                form.ShowError("У вас немає прав на редагування БД!");
            }
        }
    }
}
