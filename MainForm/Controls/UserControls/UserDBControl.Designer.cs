﻿namespace MainForm
{
    partial class UserDBControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.UserPublicIDLabel = new System.Windows.Forms.Label();
            this.UserTypeLabel = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AgentsListBox = new System.Windows.Forms.ListBox();
            this.PlayersListBox = new System.Windows.Forms.ListBox();
            this.ClubsListBox = new System.Windows.Forms.ListBox();
            this.AgentsLabel = new System.Windows.Forms.Label();
            this.PlayersLabel = new System.Windows.Forms.Label();
            this.ClubsLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).BeginInit();
            this.flowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AvatarPictureBox
            // 
            this.AvatarPictureBox.Location = new System.Drawing.Point(241, 15);
            this.AvatarPictureBox.Name = "AvatarPictureBox";
            this.AvatarPictureBox.Size = new System.Drawing.Size(113, 88);
            this.AvatarPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AvatarPictureBox.TabIndex = 7;
            this.AvatarPictureBox.TabStop = false;
            // 
            // UserPublicIDLabel
            // 
            this.UserPublicIDLabel.AutoSize = true;
            this.UserPublicIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UserPublicIDLabel.Location = new System.Drawing.Point(84, 76);
            this.UserPublicIDLabel.Name = "UserPublicIDLabel";
            this.UserPublicIDLabel.Size = new System.Drawing.Size(129, 18);
            this.UserPublicIDLabel.TabIndex = 6;
            this.UserPublicIDLabel.Text = "UserPublicIDLabel";
            // 
            // UserTypeLabel
            // 
            this.UserTypeLabel.AutoSize = true;
            this.UserTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UserTypeLabel.Location = new System.Drawing.Point(95, 104);
            this.UserTypeLabel.Name = "UserTypeLabel";
            this.UserTypeLabel.Size = new System.Drawing.Size(107, 18);
            this.UserTypeLabel.TabIndex = 5;
            this.UserTypeLabel.Text = "UserTypeLabel";
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UsernameLabel.Location = new System.Drawing.Point(16, 15);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(141, 24);
            this.UsernameLabel.TabIndex = 4;
            this.UsernameLabel.Text = "usernameLabel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(58, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(255, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "Пов\'язані з цим користувачем дані:";
            // 
            // AgentsListBox
            // 
            this.AgentsListBox.FormattingEnabled = true;
            this.AgentsListBox.Location = new System.Drawing.Point(215, 21);
            this.AgentsListBox.Name = "AgentsListBox";
            this.AgentsListBox.Size = new System.Drawing.Size(100, 69);
            this.AgentsListBox.TabIndex = 10;
            this.AgentsListBox.Visible = false;
            // 
            // PlayersListBox
            // 
            this.PlayersListBox.FormattingEnabled = true;
            this.PlayersListBox.Location = new System.Drawing.Point(109, 21);
            this.PlayersListBox.Name = "PlayersListBox";
            this.PlayersListBox.Size = new System.Drawing.Size(100, 69);
            this.PlayersListBox.TabIndex = 9;
            this.PlayersListBox.Visible = false;
            // 
            // ClubsListBox
            // 
            this.ClubsListBox.FormattingEnabled = true;
            this.ClubsListBox.Location = new System.Drawing.Point(3, 21);
            this.ClubsListBox.Name = "ClubsListBox";
            this.ClubsListBox.Size = new System.Drawing.Size(100, 69);
            this.ClubsListBox.TabIndex = 8;
            this.ClubsListBox.Visible = false;
            // 
            // AgentsLabel
            // 
            this.AgentsLabel.AutoSize = true;
            this.AgentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AgentsLabel.Location = new System.Drawing.Point(214, 0);
            this.AgentsLabel.Name = "AgentsLabel";
            this.AgentsLabel.Size = new System.Drawing.Size(102, 18);
            this.AgentsLabel.TabIndex = 14;
            this.AgentsLabel.Text = "Агенти            ";
            this.AgentsLabel.Visible = false;
            // 
            // PlayersLabel
            // 
            this.PlayersLabel.AutoSize = true;
            this.PlayersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PlayersLabel.Location = new System.Drawing.Point(108, 0);
            this.PlayersLabel.Name = "PlayersLabel";
            this.PlayersLabel.Size = new System.Drawing.Size(100, 18);
            this.PlayersLabel.TabIndex = 13;
            this.PlayersLabel.Text = "Гравці            ";
            this.PlayersLabel.Visible = false;
            // 
            // ClubsLabel
            // 
            this.ClubsLabel.AutoSize = true;
            this.ClubsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClubsLabel.Location = new System.Drawing.Point(3, 0);
            this.ClubsLabel.Name = "ClubsLabel";
            this.ClubsLabel.Size = new System.Drawing.Size(99, 18);
            this.ClubsLabel.TabIndex = 12;
            this.ClubsLabel.Text = "Клуби            ";
            this.ClubsLabel.Visible = false;
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Controls.Add(this.ClubsLabel);
            this.flowLayoutPanel.Controls.Add(this.PlayersLabel);
            this.flowLayoutPanel.Controls.Add(this.AgentsLabel);
            this.flowLayoutPanel.Controls.Add(this.ClubsListBox);
            this.flowLayoutPanel.Controls.Add(this.PlayersListBox);
            this.flowLayoutPanel.Controls.Add(this.AgentsListBox);
            this.flowLayoutPanel.Location = new System.Drawing.Point(20, 152);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(324, 99);
            this.flowLayoutPanel.TabIndex = 11;
            this.flowLayoutPanel.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(17, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "Публічний айді користувача:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(29, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 18);
            this.label2.TabIndex = 14;
            this.label2.Text = "Статус:";
            // 
            // UserDBControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.AvatarPictureBox);
            this.Controls.Add(this.UserPublicIDLabel);
            this.Controls.Add(this.UserTypeLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Name = "UserDBControl";
            this.Size = new System.Drawing.Size(362, 254);
            this.DoubleClick += new System.EventHandler(this.UserDBControl_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).EndInit();
            this.flowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox AvatarPictureBox;
        private System.Windows.Forms.Label UserPublicIDLabel;
        private System.Windows.Forms.Label UserTypeLabel;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox AgentsListBox;
        private System.Windows.Forms.ListBox PlayersListBox;
        private System.Windows.Forms.ListBox ClubsListBox;
        private System.Windows.Forms.Label AgentsLabel;
        private System.Windows.Forms.Label PlayersLabel;
        private System.Windows.Forms.Label ClubsLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
