﻿namespace MainForm.Controls.UserControls
{
    partial class UserDBChangeAddControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.selectPhotoButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.UsernameTextBox = new System.Windows.Forms.TextBox();
            this.ChangeAddButton = new System.Windows.Forms.Button();
            this.UserTypeLabel = new System.Windows.Forms.Label();
            this.UserTypeComboBox = new System.Windows.Forms.ComboBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.UserPublicIDTextBox = new System.Windows.Forms.TextBox();
            this.UserPublicIDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // AvatarPictureBox
            // 
            this.AvatarPictureBox.Location = new System.Drawing.Point(326, 60);
            this.AvatarPictureBox.Name = "AvatarPictureBox";
            this.AvatarPictureBox.Size = new System.Drawing.Size(166, 145);
            this.AvatarPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AvatarPictureBox.TabIndex = 24;
            this.AvatarPictureBox.TabStop = false;
            // 
            // selectPhotoButton
            // 
            this.selectPhotoButton.Location = new System.Drawing.Point(340, 211);
            this.selectPhotoButton.Name = "selectPhotoButton";
            this.selectPhotoButton.Size = new System.Drawing.Size(134, 23);
            this.selectPhotoButton.TabIndex = 23;
            this.selectPhotoButton.Text = "Вибрати аватар";
            this.selectPhotoButton.UseVisualStyleBackColor = true;
            this.selectPhotoButton.Click += new System.EventHandler(this.selectPhotoButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(382, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Аватар:";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(69, 133);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(81, 13);
            this.PasswordLabel.TabIndex = 19;
            this.PasswordLabel.Text = "Новий пароль:";
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Location = new System.Drawing.Point(80, 101);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(70, 13);
            this.UsernameLabel.TabIndex = 18;
            this.UsernameLabel.Text = "Новий логін:";
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(165, 130);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(121, 20);
            this.PasswordTextBox.TabIndex = 15;
            // 
            // UsernameTextBox
            // 
            this.UsernameTextBox.Location = new System.Drawing.Point(165, 98);
            this.UsernameTextBox.Name = "UsernameTextBox";
            this.UsernameTextBox.Size = new System.Drawing.Size(121, 20);
            this.UsernameTextBox.TabIndex = 14;
            // 
            // ChangeAddButton
            // 
            this.ChangeAddButton.Location = new System.Drawing.Point(165, 272);
            this.ChangeAddButton.Name = "ChangeAddButton";
            this.ChangeAddButton.Size = new System.Drawing.Size(134, 23);
            this.ChangeAddButton.TabIndex = 13;
            this.ChangeAddButton.Text = "Додати користувача";
            this.ChangeAddButton.UseVisualStyleBackColor = true;
            this.ChangeAddButton.Click += new System.EventHandler(this.ChangeAddButton_Click);
            // 
            // UserTypeLabel
            // 
            this.UserTypeLabel.AutoSize = true;
            this.UserTypeLabel.Location = new System.Drawing.Point(69, 178);
            this.UserTypeLabel.Name = "UserTypeLabel";
            this.UserTypeLabel.Size = new System.Drawing.Size(78, 13);
            this.UserTypeLabel.TabIndex = 25;
            this.UserTypeLabel.Text = "Новий статус:";
            // 
            // UserTypeComboBox
            // 
            this.UserTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UserTypeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UserTypeComboBox.FormattingEnabled = true;
            this.UserTypeComboBox.Items.AddRange(new object[] {
            "regular_user",
            "club",
            "player",
            "agent",
            "administrator"});
            this.UserTypeComboBox.Location = new System.Drawing.Point(165, 175);
            this.UserTypeComboBox.Name = "UserTypeComboBox";
            this.UserTypeComboBox.Size = new System.Drawing.Size(121, 21);
            this.UserTypeComboBox.TabIndex = 26;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(487, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(37, 18);
            this.CloseButton.TabIndex = 27;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // UserPublicIDTextBox
            // 
            this.UserPublicIDTextBox.Location = new System.Drawing.Point(165, 64);
            this.UserPublicIDTextBox.Name = "UserPublicIDTextBox";
            this.UserPublicIDTextBox.ReadOnly = true;
            this.UserPublicIDTextBox.Size = new System.Drawing.Size(121, 20);
            this.UserPublicIDTextBox.TabIndex = 28;
            // 
            // UserPublicIDLabel
            // 
            this.UserPublicIDLabel.AutoSize = true;
            this.UserPublicIDLabel.Location = new System.Drawing.Point(73, 67);
            this.UserPublicIDLabel.Name = "UserPublicIDLabel";
            this.UserPublicIDLabel.Size = new System.Drawing.Size(74, 13);
            this.UserPublicIDLabel.TabIndex = 29;
            this.UserPublicIDLabel.Text = "Публічний ID:";
            // 
            // UserDBChangeAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.UserPublicIDLabel);
            this.Controls.Add(this.UserPublicIDTextBox);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.UserTypeComboBox);
            this.Controls.Add(this.UserTypeLabel);
            this.Controls.Add(this.AvatarPictureBox);
            this.Controls.Add(this.selectPhotoButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.PasswordTextBox);
            this.Controls.Add(this.UsernameTextBox);
            this.Controls.Add(this.ChangeAddButton);
            this.Name = "UserDBChangeAddControl";
            this.Size = new System.Drawing.Size(524, 372);
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox AvatarPictureBox;
        private System.Windows.Forms.Button selectPhotoButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.TextBox UsernameTextBox;
        private System.Windows.Forms.Button ChangeAddButton;
        private System.Windows.Forms.Label UserTypeLabel;
        private System.Windows.Forms.ComboBox UserTypeComboBox;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.TextBox UserPublicIDTextBox;
        private System.Windows.Forms.Label UserPublicIDLabel;
    }
}
