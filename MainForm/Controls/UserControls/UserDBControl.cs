﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class UserDBControl : UserControl
    {
        private User _user;
        private List<Club> _clubs;
        private List<Player> _players;
        private List<Agent> _agents;
  

        public UserDBControl(User user, List<Club> clubs, List<Player> players, List<Agent> agents)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _user = user;
            _clubs = clubs;
            _players = players;
            _agents = agents;
            
            LoadData();
        }

        private void LoadData()
        {
            // Завантажити дані користувача та пов'язані дані у інших таблицях
            UsernameLabel.Text = _user.Username;
            UserPublicIDLabel.Text = _user.UserPublicID;
            UserTypeLabel.Text = _user.UserType;
            switch (_user.UserType)
            {
                case "regular_user":
                    UserTypeLabel.BackColor = Color.LightGray; // Дуже світлий сірий
                    break;
                case "club":
                    UserTypeLabel.BackColor = Color.LightGreen; // Дуже світлий зелений
                    break;
                case "player":
                    UserTypeLabel.BackColor = Color.LightSkyBlue; // Дуже світлий блакитний
                    break;
                case "agent":
                    UserTypeLabel.BackColor = Color.LightGoldenrodYellow; // Дуже світлий жовтий
                    break;
                case "administrator":
                    UserTypeLabel.BackColor = Color.LightPink; // Дуже світлий рожевий
                    break;
                default:
                    UserTypeLabel.BackColor = Color.Transparent; // За замовчуванням прозорий
                    break;
            }
            UserTypeLabel.Text = _user.UserType;

            if (_user.Avatar != null)
            {
                using (var ms = new MemoryStream(_user.Avatar))
                {
                    AvatarPictureBox.Image = Image.FromStream(ms);
                }
            }

            // Якщо хоча б один із списків містить дані, зробити панель видимою
            if ((_clubs != null && _clubs.Count > 0) || (_players != null && _players.Count > 0) || (_agents != null && _agents.Count > 0))
            {
                flowLayoutPanel.Visible = true;
            }

            if (_clubs != null && _clubs.Count > 0)
            {
                ClubsLabel.Visible = true;
                ClubsListBox.Visible = true;

                foreach (var club in _clubs)
                {
                    ClubsListBox.Items.Add(club.ClubName);
                }
            }

            if (_players != null && _players.Count > 0)
            {
                PlayersLabel.Visible = true;
                PlayersListBox.Visible = true;

                foreach (var player in _players)
                {
                    PlayersListBox.Items.Add(player.PlayerName + " " + player.PlayerSecondName);
                }
            }

            if (_agents != null && _agents.Count > 0)
            {
                AgentsLabel.Visible = true;
                AgentsListBox.Visible = true;

                foreach (var agent in _agents)
                {
                    AgentsListBox.Items.Add(agent.AgentName);
                }
            }
        }

        private void UserDBControl_DoubleClick(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && form.CurrentUser.UserType == "administrator")
            {
                form.ShowUserDBChangeAddControl(_user);
            }
        }
    }
}
