﻿namespace MainForm
{
    partial class AgentControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.photoPictureBox = new System.Windows.Forms.PictureBox();
            this.agentNameLabel = new System.Windows.Forms.Label();
            this.agencyLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.photoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // photoPictureBox
            // 
            this.photoPictureBox.Location = new System.Drawing.Point(173, 0);
            this.photoPictureBox.Name = "photoPictureBox";
            this.photoPictureBox.Size = new System.Drawing.Size(90, 76);
            this.photoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.photoPictureBox.TabIndex = 9;
            this.photoPictureBox.TabStop = false;
            // 
            // agentNameLabel
            // 
            this.agentNameLabel.AutoSize = true;
            this.agentNameLabel.Location = new System.Drawing.Point(38, 13);
            this.agentNameLabel.Name = "agentNameLabel";
            this.agentNameLabel.Size = new System.Drawing.Size(88, 13);
            this.agentNameLabel.TabIndex = 8;
            this.agentNameLabel.Text = "agentNameLabel";
            // 
            // agencyLabel
            // 
            this.agencyLabel.AutoSize = true;
            this.agencyLabel.Location = new System.Drawing.Point(58, 47);
            this.agencyLabel.Name = "agencyLabel";
            this.agencyLabel.Size = new System.Drawing.Size(68, 13);
            this.agencyLabel.TabIndex = 7;
            this.agencyLabel.Text = "agencyLabel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Ім\'я:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Агенство:";
            // 
            // AgentControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.photoPictureBox);
            this.Controls.Add(this.agentNameLabel);
            this.Controls.Add(this.agencyLabel);
            this.Name = "AgentControl";
            this.Size = new System.Drawing.Size(263, 76);
            this.DoubleClick += new System.EventHandler(this.AgentControl_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.photoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox photoPictureBox;
        private System.Windows.Forms.Label agentNameLabel;
        private System.Windows.Forms.Label agencyLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
