﻿namespace MainForm.Controls.AgentControls
{
    partial class AgentChangeAddControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserPublicIDLabel = new System.Windows.Forms.Label();
            this.UserPublicIDTextBox = new System.Windows.Forms.TextBox();
            this.PhotoPictureBox = new System.Windows.Forms.PictureBox();
            this.selectPhotoButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.AgencyLabel = new System.Windows.Forms.Label();
            this.AgentNameLabel = new System.Windows.Forms.Label();
            this.AgencyTextBox = new System.Windows.Forms.TextBox();
            this.AgentNameTextBox = new System.Windows.Forms.TextBox();
            this.ChangeAddButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PhotoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // UserPublicIDLabel
            // 
            this.UserPublicIDLabel.AutoSize = true;
            this.UserPublicIDLabel.Location = new System.Drawing.Point(17, 198);
            this.UserPublicIDLabel.Name = "UserPublicIDLabel";
            this.UserPublicIDLabel.Size = new System.Drawing.Size(140, 13);
            this.UserPublicIDLabel.TabIndex = 55;
            this.UserPublicIDLabel.Text = "Публічний ID користувача:";
            // 
            // UserPublicIDTextBox
            // 
            this.UserPublicIDTextBox.Location = new System.Drawing.Point(163, 195);
            this.UserPublicIDTextBox.Name = "UserPublicIDTextBox";
            this.UserPublicIDTextBox.Size = new System.Drawing.Size(121, 20);
            this.UserPublicIDTextBox.TabIndex = 54;
            // 
            // PhotoPictureBox
            // 
            this.PhotoPictureBox.Location = new System.Drawing.Point(295, 44);
            this.PhotoPictureBox.Name = "PhotoPictureBox";
            this.PhotoPictureBox.Size = new System.Drawing.Size(166, 145);
            this.PhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PhotoPictureBox.TabIndex = 53;
            this.PhotoPictureBox.TabStop = false;
            // 
            // selectPhotoButton
            // 
            this.selectPhotoButton.Location = new System.Drawing.Point(309, 195);
            this.selectPhotoButton.Name = "selectPhotoButton";
            this.selectPhotoButton.Size = new System.Drawing.Size(134, 23);
            this.selectPhotoButton.TabIndex = 52;
            this.selectPhotoButton.Text = "Вибрати аватар";
            this.selectPhotoButton.UseVisualStyleBackColor = true;
            this.selectPhotoButton.Click += new System.EventHandler(this.selectPhotoButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(351, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Аватар:";
            // 
            // AgencyLabel
            // 
            this.AgencyLabel.AutoSize = true;
            this.AgencyLabel.Location = new System.Drawing.Point(16, 104);
            this.AgencyLabel.Name = "AgencyLabel";
            this.AgencyLabel.Size = new System.Drawing.Size(69, 13);
            this.AgencyLabel.TabIndex = 50;
            this.AgencyLabel.Text = "AgencyLabel";
            // 
            // AgentNameLabel
            // 
            this.AgentNameLabel.AutoSize = true;
            this.AgentNameLabel.Location = new System.Drawing.Point(16, 72);
            this.AgentNameLabel.Name = "AgentNameLabel";
            this.AgentNameLabel.Size = new System.Drawing.Size(89, 13);
            this.AgentNameLabel.TabIndex = 49;
            this.AgentNameLabel.Text = "AgentNameLabel";
            // 
            // AgencyTextBox
            // 
            this.AgencyTextBox.Location = new System.Drawing.Point(131, 101);
            this.AgencyTextBox.Name = "AgencyTextBox";
            this.AgencyTextBox.Size = new System.Drawing.Size(121, 20);
            this.AgencyTextBox.TabIndex = 48;
            // 
            // AgentNameTextBox
            // 
            this.AgentNameTextBox.Location = new System.Drawing.Point(131, 69);
            this.AgentNameTextBox.Name = "AgentNameTextBox";
            this.AgentNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.AgentNameTextBox.TabIndex = 47;
            // 
            // ChangeAddButton
            // 
            this.ChangeAddButton.Location = new System.Drawing.Point(195, 260);
            this.ChangeAddButton.Name = "ChangeAddButton";
            this.ChangeAddButton.Size = new System.Drawing.Size(134, 23);
            this.ChangeAddButton.TabIndex = 46;
            this.ChangeAddButton.Text = "Додати користувача";
            this.ChangeAddButton.UseVisualStyleBackColor = true;
            this.ChangeAddButton.Click += new System.EventHandler(this.ChangeAddButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(450, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(37, 18);
            this.CloseButton.TabIndex = 56;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // AgentChangeAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.UserPublicIDLabel);
            this.Controls.Add(this.UserPublicIDTextBox);
            this.Controls.Add(this.PhotoPictureBox);
            this.Controls.Add(this.selectPhotoButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgencyLabel);
            this.Controls.Add(this.AgentNameLabel);
            this.Controls.Add(this.AgencyTextBox);
            this.Controls.Add(this.AgentNameTextBox);
            this.Controls.Add(this.ChangeAddButton);
            this.Name = "AgentChangeAddControl";
            this.Size = new System.Drawing.Size(487, 298);
            this.Load += new System.EventHandler(this.AgentChangeAddControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PhotoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label UserPublicIDLabel;
        private System.Windows.Forms.TextBox UserPublicIDTextBox;
        private System.Windows.Forms.PictureBox PhotoPictureBox;
        private System.Windows.Forms.Button selectPhotoButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label AgencyLabel;
        private System.Windows.Forms.Label AgentNameLabel;
        private System.Windows.Forms.TextBox AgencyTextBox;
        private System.Windows.Forms.TextBox AgentNameTextBox;
        private System.Windows.Forms.Button ChangeAddButton;
        private System.Windows.Forms.Button CloseButton;
    }
}
