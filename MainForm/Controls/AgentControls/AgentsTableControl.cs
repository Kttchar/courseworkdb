﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class AgentsTableControl : UserControl
    {
        public AgentsTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Отримати всіх агентів з таблиці агентів
                var agents = context.Agents.ToList();

                // Очистити FlowLayoutPanel
                AgentsTableFlowPanel.Controls.Clear();

                // Створити новий AgentControl для кожного агента і додати його до FlowLayoutPanel
                string searchText = SearchTextBox.Text.ToLower();

                foreach (var agent in agents)
                {
                    if (string.IsNullOrEmpty(searchText) ||
                        agent.AgentName.ToLower().Contains(searchText) ||
                        agent.Agency.ToLower().Contains(searchText))
                    {
                        AgentControl agentControl = new AgentControl(agent);
                        AgentsTableFlowPanel.Controls.Add(agentControl);
                    }
                }
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void AgentsTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                if (form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" ||
                    (form.CurrentUser.UserType == "agent" && !context.Agents.Any(a => a.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    form.ShowAgentChangeAddControl();
                }
                else
                {
                    form.ShowError("У вас немає прав на додавання нових агентів!");
                    return;
                }
            }
        }
    }
}