﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm.Controls.AgentControls
{
    public partial class AgentChangeAddControl : UserControl
    {
        private Agent _agent;

        public AgentChangeAddControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            // Зміна тексту лейбелів та кнопки
            AgentNameLabel.Text = "Нове ім'я:";
            AgencyLabel.Text = "Нове агентство:";
            ChangeAddButton.Text = "Додати агента";
        }

        public AgentChangeAddControl(Agent agent) : this()
        {
            _agent = agent;

            // Змінюємо текст лейбелів та кнопки
            AgentNameLabel.Text = "Змінити ім'я:";
            AgencyLabel.Text = "Змінити агентство:";
            ChangeAddButton.Text = "Змінити дані агента";

            // Вставляємо дані агента в текстбокси
            AgentNameTextBox.Text = _agent.AgentName;
            AgencyTextBox.Text = _agent.Agency;
            UserPublicIDTextBox.Text = _agent.UserPublicID;

            if (_agent.Photo != null)
            {
                using (var ms = new MemoryStream(_agent.Photo))
                {
                    PhotoPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private async void ChangeAddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Перевірка при додаванні нового агента
                if (_agent == null && !(form.CurrentUser.UserType == "administrator" ||
                    (form.CurrentUser.UserType == "agent" && !context.Agents.Any(a => a.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    form.ShowError("У вас немає прав на додавання нових агентів!");
                    return;
                }

                // Перевірка при редагуванні агента
                if (_agent != null && !(form.CurrentUser.UserType == "administrator" || (_agent != null && form.CurrentUser.UserType == "agent" && form.CurrentUser.UserPublicID == _agent.UserPublicID)))
                {
                    form.ShowError("У вас немає прав на редагування цього агента!");
                    return;
                }

                // Перевірка, чи всі поля заповнені
                if (string.IsNullOrEmpty(AgentNameTextBox.Text) ||
                    string.IsNullOrEmpty(AgencyTextBox.Text) ||
                    string.IsNullOrEmpty(UserPublicIDTextBox.Text))
                {
                    form.ShowError("Будь ласка, заповніть всі поля!");
                    return;
                }

                byte[] photoBytes = null;
                if (PhotoPictureBox.Image != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        PhotoPictureBox.Image.Save(ms, PhotoPictureBox.Image.RawFormat);
                        photoBytes = ms.ToArray();
                    }
                }

                // Перевірка, чи існує користувач з таким публічним айді
                var existingUser = await context.Users.FirstOrDefaultAsync(u => u.UserPublicID == UserPublicIDTextBox.Text);
                if (existingUser == null)
                {
                    form.ShowError("Користувача з таким публічним ID не існує!");
                    return;
                }

                // Перевірка, чи існує агент з таким ім'ям
                var existingAgent = await context.Agents.FirstOrDefaultAsync(a => a.AgentName == AgentNameTextBox.Text);
                if (existingAgent != null && (_agent == null || existingAgent.AgentID != _agent.AgentID))
                {
                    form.ShowError("Агент з таким ім'ям вже існує!");
                    return;
                }

                if (_agent == null)
                {
                    // Виклик процедури AddAgentByUserID
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC AddAgentByUserID @p0, @p1, @p2, @p3",
                        AgentNameTextBox.Text,
                        AgencyTextBox.Text,
                        photoBytes,
                        UserPublicIDTextBox.Text
                    );
                    form.ShowInfo("Агент успішно доданий!");
                }
                else
                {
                    // Виклик процедури ChangeAgentData
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC ChangeAgentData @p0, @p1, @p2, @p3, @p4",
                        _agent.AgentID,
                        AgentNameTextBox.Text,
                        AgencyTextBox.Text,
                        photoBytes,
                        UserPublicIDTextBox.Text
                    );
                    form.ShowInfo("Дані агента успішно змінено!");
                }
            }
        }

        private void AgentChangeAddControl_Load(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && _agent ==null)
                UserPublicIDTextBox.Text = form.CurrentUser.UserPublicID;
        }

        private void selectPhotoButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Image Files( *.gif; *.bmp; *.png)|*.gif; *.bmp; *.png"; 
                if (ofd.ShowDialog() == DialogResult.OK)
                {


                    PhotoPictureBox.Image = Image.FromFile(ofd.FileName);
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
