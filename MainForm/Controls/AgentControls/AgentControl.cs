﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class AgentControl : UserControl
    {

        private Agent _agent;
        public AgentControl(Agent agent)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _agent = agent;

            agentNameLabel.Text = _agent.AgentName;
            agencyLabel.Text = _agent.Agency;

            if (_agent.Photo != null)
            {
                using (var ms = new MemoryStream(_agent.Photo))
                {
                    photoPictureBox.Image = Image.FromStream(ms);
                }
            }
        }

        private void AgentControl_DoubleClick(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            if (form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" || (_agent != null && form.CurrentUser.UserType == "agent" && form.CurrentUser.UserPublicID == _agent.UserPublicID)))
            {
                form.ShowAgentChangeAddControl(_agent);
            }
        }
    }
}
