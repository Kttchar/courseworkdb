﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class TransferControl : UserControl
    {

        private Transfer _transfer;
        private string _playerName;
        private string _fromClubName;
        private string _toClubName;

        public TransferControl(Transfer transfer, string playerName, string fromClubName, string toClubName)
        {
            InitializeComponent();

            this.BackColor = Color.WhiteSmoke;

            _transfer = transfer;
            _playerName = playerName;
            _fromClubName = fromClubName;
            _toClubName = toClubName;

            playerNameLabel.Text = _playerName;
            fromClubLabel.Text = _fromClubName;
            toClubLabel.Text = _toClubName;
            transferFeeLabel.Text = _transfer.TransferFee.ToString();
            transferDateLabel.Text = _transfer.TransferDate.ToShortDateString();
            transferStatusLabel.Text = _transfer.TransferStatus;
            switch (_transfer.TransferStatus)
            {
                case "в процесі":
                    transferStatusLabel.BackColor = Color.Yellow;
                    break;
                case "завершено":
                    transferStatusLabel.BackColor = Color.LightGreen;
                    break;
                case "скасовано":
                    transferStatusLabel.BackColor = Color.Salmon; // Світліший червоний
                    break;
                default:
                    transferStatusLabel.BackColor = Color.Gray; // Колір за замовчуванням
                    break;
            }
            transferStatusLabel.Text = _transfer.TransferStatus;

        }

        private void TransferControl_DoubleClick(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            
            form.ShowTransferChangeAddControl(_transfer);

        }
    }
}
