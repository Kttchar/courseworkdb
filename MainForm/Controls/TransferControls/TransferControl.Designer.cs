﻿namespace MainForm
{
    partial class TransferControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.playerNameLabel = new System.Windows.Forms.Label();
            this.fromClubLabel = new System.Windows.Forms.Label();
            this.transferFeeLabel = new System.Windows.Forms.Label();
            this.toClubLabel = new System.Windows.Forms.Label();
            this.transferDateLabel = new System.Windows.Forms.Label();
            this.transferStatusLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // playerNameLabel
            // 
            this.playerNameLabel.AutoSize = true;
            this.playerNameLabel.Location = new System.Drawing.Point(116, 11);
            this.playerNameLabel.Name = "playerNameLabel";
            this.playerNameLabel.Size = new System.Drawing.Size(89, 13);
            this.playerNameLabel.TabIndex = 8;
            this.playerNameLabel.Text = "playerNameLabel";
            // 
            // fromClubLabel
            // 
            this.fromClubLabel.AutoSize = true;
            this.fromClubLabel.Location = new System.Drawing.Point(24, 67);
            this.fromClubLabel.Name = "fromClubLabel";
            this.fromClubLabel.Size = new System.Drawing.Size(74, 13);
            this.fromClubLabel.TabIndex = 7;
            this.fromClubLabel.Text = "fromClubLabel";
            // 
            // transferFeeLabel
            // 
            this.transferFeeLabel.AutoSize = true;
            this.transferFeeLabel.Location = new System.Drawing.Point(125, 138);
            this.transferFeeLabel.Name = "transferFeeLabel";
            this.transferFeeLabel.Size = new System.Drawing.Size(86, 13);
            this.transferFeeLabel.TabIndex = 6;
            this.transferFeeLabel.Text = "transferFeeLabel";
            // 
            // toClubLabel
            // 
            this.toClubLabel.AutoSize = true;
            this.toClubLabel.Location = new System.Drawing.Point(146, 67);
            this.toClubLabel.Name = "toClubLabel";
            this.toClubLabel.Size = new System.Drawing.Size(63, 13);
            this.toClubLabel.TabIndex = 5;
            this.toClubLabel.Text = "toClubLabel";
            // 
            // transferDateLabel
            // 
            this.transferDateLabel.AutoSize = true;
            this.transferDateLabel.Location = new System.Drawing.Point(128, 95);
            this.transferDateLabel.Name = "transferDateLabel";
            this.transferDateLabel.Size = new System.Drawing.Size(91, 13);
            this.transferDateLabel.TabIndex = 9;
            this.transferDateLabel.Text = "transferDateLabel";
            // 
            // transferStatusLabel
            // 
            this.transferStatusLabel.AutoSize = true;
            this.transferStatusLabel.Location = new System.Drawing.Point(125, 118);
            this.transferStatusLabel.Name = "transferStatusLabel";
            this.transferStatusLabel.Size = new System.Drawing.Size(98, 13);
            this.transferStatusLabel.TabIndex = 10;
            this.transferStatusLabel.Text = "transferStatusLabel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Футболіст:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "З клубу:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(146, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "В клуб:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Дата:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Статус:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Вартість трансферу:";
            // 
            // TransferControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.transferStatusLabel);
            this.Controls.Add(this.transferDateLabel);
            this.Controls.Add(this.playerNameLabel);
            this.Controls.Add(this.fromClubLabel);
            this.Controls.Add(this.transferFeeLabel);
            this.Controls.Add(this.toClubLabel);
            this.Name = "TransferControl";
            this.Size = new System.Drawing.Size(260, 156);
            this.DoubleClick += new System.EventHandler(this.TransferControl_DoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label playerNameLabel;
        private System.Windows.Forms.Label fromClubLabel;
        private System.Windows.Forms.Label transferFeeLabel;
        private System.Windows.Forms.Label toClubLabel;
        private System.Windows.Forms.Label transferDateLabel;
        private System.Windows.Forms.Label transferStatusLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
