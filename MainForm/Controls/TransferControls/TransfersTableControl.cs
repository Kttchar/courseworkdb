﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    public partial class TransfersTableControl : UserControl
    {
        public TransfersTableControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void UpdateTable()
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {
                // Отримати всі трансфери з таблиці трансферів
                var transfers = context.Transfers.ToList();

                // Очистити FlowLayoutPanel
                TransfersTableFlowPanel.Controls.Clear();

                // Створити новий TransferControl для кожного трансферу і додати його до FlowLayoutPanel
                string searchText = SearchTextBox.Text.ToLower();

                foreach (var transfer in transfers)
                {
                    // Отримати гравця та клуби за PlayerID, FromClubID та ToClubID
                    var player = context.Players.FirstOrDefault(p => p.PlayerID == transfer.PlayerID);
                    var fromClub = context.Clubs.FirstOrDefault(c => c.ClubID == transfer.FromClubID);
                    var toClub = context.Clubs.FirstOrDefault(c => c.ClubID == transfer.ToClubID);

                    string playerName = player != null ? player.PlayerName + " " + player.PlayerSecondName : "";
                    string fromClubName = fromClub != null ? fromClub.ClubName : "";
                    string toClubName = toClub != null ? toClub.ClubName : "";

                    if (string.IsNullOrEmpty(searchText) ||
                        playerName.ToLower().Contains(searchText) ||
                        fromClubName.ToLower().Contains(searchText) ||
                        toClubName.ToLower().Contains(searchText) ||
                        transfer.TransferStatus.ToLower().Contains(searchText))
                    {
                        TransferControl transferControl = new TransferControl(transfer, playerName, fromClubName, toClubName); // передати імена гравця та клубів до TransferControl
                        TransfersTableFlowPanel.Controls.Add(transferControl);
                    }
                }
            }
        }



        private void TransfersTableControl_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB;
            
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {

                if (form.CurrentUser != null && form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))
                    form.ShowTransferChangeAddControl();
                else
                {
                    form.ShowError("У вас немає прав на додавання нових трансферів!");
                }

            }
        }
    }
}
