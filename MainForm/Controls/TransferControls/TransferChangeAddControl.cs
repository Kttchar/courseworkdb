﻿using FC_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm.Controls.TransferControls
{
    public partial class TransferChangeAddControl : UserControl
    {
        private Transfer _transfer;

        public TransferChangeAddControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            // Зміна тексту лейбелів та кнопки
            PlayerNameLabel.Text = "Ім'я гравця:";
            PlayerSecondNameLabel.Text = "Прізвище гравця:";
            FromClubNameLabel.Text = "Клуб відправника:";
            ToClubNameLabel.Text = "Клуб отримувач:";
            TransferFeeLabel.Text = "Сума трансферу:";
            TransferDateLabel.Text = "Дата трансферу:";
            TransferStatusLabel.Text = "Статус трансферу:";
            ChangeAddButton.Text = "Додати трансфер";
        }

        public TransferChangeAddControl(Transfer transfer) : this()
        {
            _transfer = transfer;

            // Змінюємо текст лейбелів та кнопки
            
            ChangeAddButton.Text = "Змінити дані трансферу";

            // Вставляємо дані трансферу в текстбокси

            TransferFeeTextBox.Text = ((decimal)_transfer.TransferFee).ToString("F0");

            TransferDatePicker.Value = _transfer.TransferDate;

            TransferStatusComboBox.SelectedItem = _transfer.TransferStatus;
        }

        private async void ChangeAddButton_Click(object sender, EventArgs e)
        {
            var form = this.FindForm() as MainFormFootballDB; // посилання на батьківську форму
            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {

                // Перевірка при додаванні нового трансферу
                if (_transfer == null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    form.ShowError("У вас немає прав на додавання нових трансферів!");
                    return;
                }

                // Перевірка при редагуванні трансферу
                if (_transfer != null && ( form.CurrentUser == null || form.CurrentUser != null &&  !(form.CurrentUser.UserType == "administrator" ||
                (_transfer != null && form.CurrentUser.UserType == "club" &&
                context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && (c.ClubID == _transfer.FromClubID || c.ClubID == _transfer.ToClubID))))))
                {
                    form.ShowError("У вас немає прав на редагування цього трансферу!");
                    return;
                }

                // Перевірка при додаванні нового трансферу
                if (_transfer == null && form.CurrentUser != null && form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))
                {
                    // Клубський акаунт клуба- відправника при створенні трансферу може встановити статус тільки "в процесі"
                    TransferStatusComboBox.SelectedItem = "в процесі";
                }

                // Перевірка при редагуванні трансферу
                if (_transfer != null && form.CurrentUser != null && form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && c.ClubID == _transfer.FromClubID))
                {
                    // Клуб який створив трансфер може змінити - це встановити статус "скасовано"
                    if (TransferStatusComboBox.SelectedItem.ToString() != "скасовано")
                    {
                        form.ShowError("Ви можете тільки скасувати трансфер!");
                        return;
                    }
                }

                // Перевірка при редагуванні трансферу
                if (_transfer != null && form.CurrentUser != null && (form.CurrentUser.UserType == "administrator" || (form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && c.ClubID == _transfer.ToClubID))))
                {
                    // Адміністратор та клубський акаунт що прив'язаний до клубу-отримувача можуть змінити - це встановити статус "скасовано" або "завершено"
                    if (TransferStatusComboBox.SelectedItem.ToString() != "скасовано" && TransferStatusComboBox.SelectedItem.ToString() != "завершено")
                    {
                        form.ShowError("Ви можете тільки скасувати або завершити трансфер!");
                        return;
                    }
                }

                // Перевірка, чи всі поля заповнені
                if (string.IsNullOrEmpty(PlayerNameTextBox.Text) ||
                string.IsNullOrEmpty(PlayerSecondNameTextBox.Text) ||
                string.IsNullOrEmpty(FromClubNameTextBox.Text) ||
                string.IsNullOrEmpty(ToClubNameTextBox.Text) ||
                string.IsNullOrEmpty(TransferFeeTextBox.Text) ||
                string.IsNullOrEmpty(TransferStatusComboBox.Text))
                {
                    form.ShowError("Будь ласка, заповніть всі поля!");
                    return;
                }

                // Перевірка, чи існує гравець з таким ім'ям
                var existingPlayer = await context.Players.FirstOrDefaultAsync(p => p.PlayerName == PlayerNameTextBox.Text && p.PlayerSecondName == PlayerSecondNameTextBox.Text);
                if (existingPlayer == null)
                {
                    form.ShowError("Гравця з таким ім'ям та прізвищем не існує!");
                    return;
                }

                // Перевірка, чи існує клуб відправник з таким ім'ям
                var existingFromClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubName == FromClubNameTextBox.Text);
                if (existingFromClub == null)
                {
                    form.ShowError("Клубу відправника з таким ім'ям не існує!");
                    return;
                }

                // Перевірка, чи існує клуб отримувач з таким ім'ям
                var existingToClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubName == ToClubNameTextBox.Text);
                if (existingToClub == null)
                {
                    form.ShowError("Клубу отримувача з таким ім'ям не існує!");
                    return;
                }

                // Перевірка, чи гравець належить клубу відправнику
                if (existingPlayer.ClubID != existingFromClub.ClubID)
                {
                    form.ShowError("Гравець не належить клубу відправнику!");
                    return;
                }

                // Перевірка, чи клуб відправник не передає гравця самому собі
                if (existingFromClub.ClubID == existingToClub.ClubID)
                {
                    form.ShowError("Клуб відправник не може передати гравця самому собі!");
                    return;
                }


                string transferFeeText = TransferFeeTextBox.Text.Replace(',', '.');
                if (!double.TryParse(transferFeeText, NumberStyles.Float, CultureInfo.InvariantCulture, out double transferFee))
                {
                    form.ShowError("Недійсне значення плати за трансфер!");
                    return;
                }

                var parameters = new[]
                {
                    new SqlParameter("@playerName", existingPlayer.PlayerName),
                    new SqlParameter("@playerSecondName", existingPlayer.PlayerSecondName),
                    new SqlParameter("@fromClubName", existingFromClub.ClubName),
                    new SqlParameter("@toClubName", existingToClub.ClubName),
                    new SqlParameter("@transferFee", SqlDbType.Float) { Value = transferFee },
                    new SqlParameter("@transferDate", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                    new SqlParameter("@transferStatus", TransferStatusComboBox.Text)
                };

                if (_transfer == null)
                {
                    // Виклик процедури AddTransfer
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC AddTransfer @playerName, @playerSecondName, @fromClubName, @toClubName, @transferFee, @transferDate, @transferStatus",
                        parameters: parameters
                    );
                    form.ShowInfo("Трансфер успішно доданий!");
                }
                else
                {
                    // Виклик процедури ChangeTransfer
                    var result = await context.Database.ExecuteSqlCommandAsync(
                        "EXEC ChangeTransfer @transferID, @playerName, @playerSecondName, @fromClubName, @toClubName, @transferFee, @transferDate, @transferStatus",
                        parameters: parameters.Prepend(new SqlParameter("@transferID", _transfer.TransferID)).ToArray()
                    );
                    form.ShowInfo("Дані трансферу успішно змінено!");
                }


                this.Visible = false;
            }
        }

        private async void TransferChangeAddControl_Load(object sender, EventArgs e)
        {



            var form = this.FindForm() as MainFormFootballDB;

            var (DBserver, DBDataBase, DBusername, DBpassword) = form.GetConnectionParameters();

            using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
            {

                // Перевірка при додаванні нового трансферу
                if (_transfer == null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "club" && context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID))))
                {
                    TransferStatusComboBox.Enabled = false;
                    ChangeAddButton.Enabled = false;
                    ChangeAddButton.Visible = false;
                }

                // Перевірка при редагуванні трансферу
                if (_transfer != null && (form.CurrentUser == null || form.CurrentUser != null && !(form.CurrentUser.UserType == "administrator" ||
                (_transfer != null && form.CurrentUser.UserType == "club" &&
                context.Clubs.Any(c => c.UserPublicID == form.CurrentUser.UserPublicID && (c.ClubID == _transfer.FromClubID || c.ClubID == _transfer.ToClubID))))))
                {
                    TransferStatusComboBox.Enabled = false;
                    ChangeAddButton.Enabled = false;
                    ChangeAddButton.Visible = false;
                }
            }

                if (_transfer != null)
            {
                // поля крім статусу - рідонлі з початку Load
                PlayerNameTextBox.ReadOnly = true;
                PlayerSecondNameTextBox.ReadOnly = true;
                FromClubNameTextBox.ReadOnly = true;
                ToClubNameTextBox.ReadOnly = true;
                TransferFeeTextBox.ReadOnly = true;
                

                // Якщо статус трансферу вже "скасовано" або "завершено" - кнопка зміни - недоступна
                if (_transfer.TransferStatus == "скасовано" || _transfer.TransferStatus == "завершено")
                {
                    TransferStatusComboBox.Enabled = false;
                    ChangeAddButton.Enabled = false;
                    ChangeAddButton.Visible = false;
                }

                

                try
                {
                    using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                    {
                        var player = await context.Players.FirstOrDefaultAsync(p => p.PlayerID == _transfer.PlayerID);
                        var fromClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubID == _transfer.FromClubID);
                        var toClub = await context.Clubs.FirstOrDefaultAsync(c => c.ClubID == _transfer.ToClubID);

                        if (player != null && fromClub != null && toClub != null)
                        {
                            PlayerNameTextBox.Text = player.PlayerName;
                            PlayerSecondNameTextBox.Text = player.PlayerSecondName;
                            FromClubNameTextBox.Text = fromClub.ClubName;
                            ToClubNameTextBox.Text = toClub.ClubName;

                            if (player.Photo != null)
                            {
                                using (var ms = new MemoryStream(player.Photo))
                                {
                                    PlayerPictureBox.Image = Image.FromStream(ms);
                                }
                            }

                            if (fromClub.Logo != null)
                            {
                                using (var ms = new MemoryStream(fromClub.Logo))
                                {
                                    FromClubPictureBox.Image = Image.FromStream(ms);
                                }
                            }

                            if (toClub.Logo != null)
                            {
                                using (var ms = new MemoryStream(toClub.Logo))
                                {
                                    ToClubPictureBox.Image = Image.FromStream(ms);
                                }
                            }
                        }
                        else
                        {
                            // Якщо гравець або клуб не знайдено, повідомлення про помилку
                            form.ShowError("Гравця або клубу з таким ID не існує!");
                        }
                    }
                }
                catch (Exception ex) { }
            }
            else if(_transfer == null )
            {
                
                using (var context = new FootballDbContext(DBserver, DBDataBase, DBusername, DBpassword))
                {
                    if (form.CurrentUser != null && form.CurrentUser.UserType == "club")
                    {


                        var fromClub = await context.Clubs.FirstOrDefaultAsync(c => c.UserPublicID == form.CurrentUser.UserPublicID);
                        FromClubNameTextBox.Text = fromClub.ClubName;
                    }
                }
            }
            
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
