﻿namespace MainForm.Controls.TransferControls
{
    partial class TransferChangeAddControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransferDateLabel = new System.Windows.Forms.Label();
            this.TransferFeeLabel = new System.Windows.Forms.Label();
            this.TransferFeeTextBox = new System.Windows.Forms.TextBox();
            this.ToClubNameLabel = new System.Windows.Forms.Label();
            this.ToClubNameTextBox = new System.Windows.Forms.TextBox();
            this.FromClubNameLabel = new System.Windows.Forms.Label();
            this.FromClubNameTextBox = new System.Windows.Forms.TextBox();
            this.PlayerPictureBox = new System.Windows.Forms.PictureBox();
            this.PlayerSecondNameLabel = new System.Windows.Forms.Label();
            this.PlayerNameLabel = new System.Windows.Forms.Label();
            this.PlayerSecondNameTextBox = new System.Windows.Forms.TextBox();
            this.PlayerNameTextBox = new System.Windows.Forms.TextBox();
            this.ChangeAddButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.TransferStatusLabel = new System.Windows.Forms.Label();
            this.TransferStatusComboBox = new System.Windows.Forms.ComboBox();
            this.FromClubPictureBox = new System.Windows.Forms.PictureBox();
            this.ToClubPictureBox = new System.Windows.Forms.PictureBox();
            this.TransferDatePicker = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromClubPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToClubPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TransferDateLabel
            // 
            this.TransferDateLabel.AutoSize = true;
            this.TransferDateLabel.Location = new System.Drawing.Point(162, 220);
            this.TransferDateLabel.Name = "TransferDateLabel";
            this.TransferDateLabel.Size = new System.Drawing.Size(95, 13);
            this.TransferDateLabel.TabIndex = 83;
            this.TransferDateLabel.Text = "TransferDateLabel";
            // 
            // TransferFeeLabel
            // 
            this.TransferFeeLabel.AutoSize = true;
            this.TransferFeeLabel.Location = new System.Drawing.Point(167, 187);
            this.TransferFeeLabel.Name = "TransferFeeLabel";
            this.TransferFeeLabel.Size = new System.Drawing.Size(90, 13);
            this.TransferFeeLabel.TabIndex = 81;
            this.TransferFeeLabel.Text = "TransferFeeLabel";
            // 
            // TransferFeeTextBox
            // 
            this.TransferFeeTextBox.Location = new System.Drawing.Point(301, 184);
            this.TransferFeeTextBox.Name = "TransferFeeTextBox";
            this.TransferFeeTextBox.Size = new System.Drawing.Size(121, 20);
            this.TransferFeeTextBox.TabIndex = 80;
            // 
            // ToClubNameLabel
            // 
            this.ToClubNameLabel.AutoSize = true;
            this.ToClubNameLabel.Location = new System.Drawing.Point(289, 150);
            this.ToClubNameLabel.Name = "ToClubNameLabel";
            this.ToClubNameLabel.Size = new System.Drawing.Size(95, 13);
            this.ToClubNameLabel.TabIndex = 79;
            this.ToClubNameLabel.Text = "ToClubNameLabel";
            // 
            // ToClubNameTextBox
            // 
            this.ToClubNameTextBox.Location = new System.Drawing.Point(411, 147);
            this.ToClubNameTextBox.Name = "ToClubNameTextBox";
            this.ToClubNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.ToClubNameTextBox.TabIndex = 78;
            // 
            // FromClubNameLabel
            // 
            this.FromClubNameLabel.AutoSize = true;
            this.FromClubNameLabel.Location = new System.Drawing.Point(21, 146);
            this.FromClubNameLabel.Name = "FromClubNameLabel";
            this.FromClubNameLabel.Size = new System.Drawing.Size(105, 13);
            this.FromClubNameLabel.TabIndex = 77;
            this.FromClubNameLabel.Text = "FromClubNameLabel";
            // 
            // FromClubNameTextBox
            // 
            this.FromClubNameTextBox.Location = new System.Drawing.Point(143, 143);
            this.FromClubNameTextBox.Name = "FromClubNameTextBox";
            this.FromClubNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.FromClubNameTextBox.TabIndex = 76;
            // 
            // PlayerPictureBox
            // 
            this.PlayerPictureBox.Location = new System.Drawing.Point(331, 16);
            this.PlayerPictureBox.Name = "PlayerPictureBox";
            this.PlayerPictureBox.Size = new System.Drawing.Size(81, 57);
            this.PlayerPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PlayerPictureBox.TabIndex = 73;
            this.PlayerPictureBox.TabStop = false;
            // 
            // PlayerSecondNameLabel
            // 
            this.PlayerSecondNameLabel.AutoSize = true;
            this.PlayerSecondNameLabel.Location = new System.Drawing.Point(54, 56);
            this.PlayerSecondNameLabel.Name = "PlayerSecondNameLabel";
            this.PlayerSecondNameLabel.Size = new System.Drawing.Size(127, 13);
            this.PlayerSecondNameLabel.TabIndex = 70;
            this.PlayerSecondNameLabel.Text = "PlayerSecondNameLabel";
            // 
            // PlayerNameLabel
            // 
            this.PlayerNameLabel.AutoSize = true;
            this.PlayerNameLabel.Location = new System.Drawing.Point(57, 24);
            this.PlayerNameLabel.Name = "PlayerNameLabel";
            this.PlayerNameLabel.Size = new System.Drawing.Size(90, 13);
            this.PlayerNameLabel.TabIndex = 69;
            this.PlayerNameLabel.Text = "PlayerNameLabel";
            // 
            // PlayerSecondNameTextBox
            // 
            this.PlayerSecondNameTextBox.Location = new System.Drawing.Point(187, 53);
            this.PlayerSecondNameTextBox.Name = "PlayerSecondNameTextBox";
            this.PlayerSecondNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.PlayerSecondNameTextBox.TabIndex = 68;
            // 
            // PlayerNameTextBox
            // 
            this.PlayerNameTextBox.Location = new System.Drawing.Point(187, 21);
            this.PlayerNameTextBox.Name = "PlayerNameTextBox";
            this.PlayerNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.PlayerNameTextBox.TabIndex = 67;
            // 
            // ChangeAddButton
            // 
            this.ChangeAddButton.Location = new System.Drawing.Point(187, 311);
            this.ChangeAddButton.Name = "ChangeAddButton";
            this.ChangeAddButton.Size = new System.Drawing.Size(161, 23);
            this.ChangeAddButton.TabIndex = 66;
            this.ChangeAddButton.Text = "Додати користувача";
            this.ChangeAddButton.UseVisualStyleBackColor = true;
            this.ChangeAddButton.Click += new System.EventHandler(this.ChangeAddButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(502, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(37, 18);
            this.CloseButton.TabIndex = 84;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // TransferStatusLabel
            // 
            this.TransferStatusLabel.AutoSize = true;
            this.TransferStatusLabel.Location = new System.Drawing.Point(162, 260);
            this.TransferStatusLabel.Name = "TransferStatusLabel";
            this.TransferStatusLabel.Size = new System.Drawing.Size(102, 13);
            this.TransferStatusLabel.TabIndex = 86;
            this.TransferStatusLabel.Text = "TransferStatusLabel";
            // 
            // TransferStatusComboBox
            // 
            this.TransferStatusComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransferStatusComboBox.FormattingEnabled = true;
            this.TransferStatusComboBox.Items.AddRange(new object[] {
            "в процесі",
            "завершено",
            "скасовано"});
            this.TransferStatusComboBox.Location = new System.Drawing.Point(301, 257);
            this.TransferStatusComboBox.Name = "TransferStatusComboBox";
            this.TransferStatusComboBox.Size = new System.Drawing.Size(121, 21);
            this.TransferStatusComboBox.TabIndex = 87;
            // 
            // FromClubPictureBox
            // 
            this.FromClubPictureBox.Location = new System.Drawing.Point(143, 83);
            this.FromClubPictureBox.Name = "FromClubPictureBox";
            this.FromClubPictureBox.Size = new System.Drawing.Size(81, 57);
            this.FromClubPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FromClubPictureBox.TabIndex = 88;
            this.FromClubPictureBox.TabStop = false;
            // 
            // ToClubPictureBox
            // 
            this.ToClubPictureBox.Location = new System.Drawing.Point(411, 83);
            this.ToClubPictureBox.Name = "ToClubPictureBox";
            this.ToClubPictureBox.Size = new System.Drawing.Size(81, 57);
            this.ToClubPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ToClubPictureBox.TabIndex = 89;
            this.ToClubPictureBox.TabStop = false;
            // 
            // TransferDatePicker
            // 
            this.TransferDatePicker.Enabled = false;
            this.TransferDatePicker.Location = new System.Drawing.Point(301, 220);
            this.TransferDatePicker.Name = "TransferDatePicker";
            this.TransferDatePicker.Size = new System.Drawing.Size(136, 20);
            this.TransferDatePicker.TabIndex = 90;
            // 
            // TransferChangeAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TransferDatePicker);
            this.Controls.Add(this.ToClubPictureBox);
            this.Controls.Add(this.FromClubPictureBox);
            this.Controls.Add(this.TransferStatusComboBox);
            this.Controls.Add(this.TransferStatusLabel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.TransferDateLabel);
            this.Controls.Add(this.TransferFeeLabel);
            this.Controls.Add(this.TransferFeeTextBox);
            this.Controls.Add(this.ToClubNameLabel);
            this.Controls.Add(this.ToClubNameTextBox);
            this.Controls.Add(this.FromClubNameLabel);
            this.Controls.Add(this.FromClubNameTextBox);
            this.Controls.Add(this.PlayerPictureBox);
            this.Controls.Add(this.PlayerSecondNameLabel);
            this.Controls.Add(this.PlayerNameLabel);
            this.Controls.Add(this.PlayerSecondNameTextBox);
            this.Controls.Add(this.PlayerNameTextBox);
            this.Controls.Add(this.ChangeAddButton);
            this.Name = "TransferChangeAddControl";
            this.Size = new System.Drawing.Size(539, 343);
            this.Load += new System.EventHandler(this.TransferChangeAddControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PlayerPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromClubPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToClubPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TransferDateLabel;
        private System.Windows.Forms.Label TransferFeeLabel;
        private System.Windows.Forms.TextBox TransferFeeTextBox;
        private System.Windows.Forms.Label ToClubNameLabel;
        private System.Windows.Forms.TextBox ToClubNameTextBox;
        private System.Windows.Forms.Label FromClubNameLabel;
        private System.Windows.Forms.TextBox FromClubNameTextBox;
        private System.Windows.Forms.PictureBox PlayerPictureBox;
        private System.Windows.Forms.Label PlayerSecondNameLabel;
        private System.Windows.Forms.Label PlayerNameLabel;
        private System.Windows.Forms.TextBox PlayerSecondNameTextBox;
        private System.Windows.Forms.TextBox PlayerNameTextBox;
        private System.Windows.Forms.Button ChangeAddButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label TransferStatusLabel;
        private System.Windows.Forms.ComboBox TransferStatusComboBox;
        private System.Windows.Forms.PictureBox FromClubPictureBox;
        private System.Windows.Forms.PictureBox ToClubPictureBox;
        private System.Windows.Forms.DateTimePicker TransferDatePicker;
    }
}
